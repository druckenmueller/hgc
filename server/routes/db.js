'use strict';

var moment = require('moment');
var pwd = require('./pwdHash');



module.exports = {
    user: {
        update: function ( db, userId, fields, cb) {
            console.log('update user ' + userId + ' fields: ' + JSON.stringify(fields));
            db.collection('user').updateOne({_id:userId},{$set: fields}, cb);
        },
        authenticate: function (req, username, password, cb) {
            console.log('user authenticate');

            var db = req.db;
            var users = db.collection('user');
            users.findOne({ username: username }, function (err, user) {
               
                if (err) {
                    return cb(err);
                }

                if (!user) {
                    var err_1 = new Error('not found');
                    err_1.status = 401;
                    return cb(err_1);
                }

                console.log('user: ' + JSON.stringify(user));

                db.collection('company').findOne({}, function(err, company) {
                    if (err) cb(err);

                    pwd.crypto.verify(user.password, password, function(err, credentialsMatch) {
                        if (err) cb(err);

                        credentialsMatch = credentialsMatch && user.active;

                        var now = new Date().toISOString();

                        if (credentialsMatch) {
                            var userUpdate = {
                                lastSuccessfulLogin : now
                            };
                        }
                        else {
                            var userUpdate = {
                                lastFailedLogin : now
                            };
                        }



                        users.updateOne(
                            { username: username }, 
                            { 
                                $set: userUpdate,
                                $push: { loginHistory: {
                                    loginDate: now,
                                    successful: credentialsMatch,
                                    browser: req.get('User-Agent'),
                                    ip: req.get('x-forwarded-for') || req.connection.remoteAddress
                                } }
                            },
                            function (err, userDates) {
                                if (err) return cb(err);
                                    
                                if (credentialsMatch) {
                                    cb(null, {
                                        id: user._id,
                                        __id: user.__id,
                                        email: user.email,
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        passwordExpired: moment().diff(user.expiryDate) > 0,
                                        lastSuccessfulLogin: user.lastSuccessfulLogin,
                                        lastFailedLogin: user.lastFailedLogin
                                    });   
                                }
                                else {
                                    cb(null, false);
                                }
                        });
                        
                        /*
                        users.update(
                            { username: username }, 
                            { $push: { loginHistory: {
                                loginDate: now,
                                successful: credentialsMatch,
                                browser: req.get('User-Agent'),
                                ip: req.get('x-forwarded-for') || req.connection.remoteAddress
                            } } }
                        );

                        users.updateOne(
                            { username: username }, 
                            { $set: userUpdate },
                            function (err, userDates) {
                                if (err) return cb(err);
                                    
                                if (credentialsMatch) {
                                    cb(null, {
                                        id: user._id,
                                        email: user.email,
                                        firstname: user.firstname,
                                        lastname: user.lastname,
                                        passwordExpired: moment().diff(user.expiryDate) > 0,
                                        lastSuccessfulLogin: user.lastSuccessfulLogin,
                                        lastFailedLogin: user.lastFailedLogin
                                    });   
                                }
                                else {
                                    cb(null, false);
                                }
                        });
                        */
                    });
                });
            });
        },
        find: function(db, query, cb) {
            db.collection('user').findOne(query, { loginHistory : 0 }, function(err, user) {
                cb(err, user);
            });
        },
        findById: function(db, userId, cb) {
            db.collection('user').findOne({_id: new ObjectId(userId)}, { loginHistory : 0 }, function(err, user) {
                cb(err, user);
            });
        }
    },
    client: {
        clients: [],
        clientCount: 0,
        updateOrCreate: function (data, cb) {
            var session = {
                userId: data.user.id,
                createdTime: new Date().toISOString()
            };
            data.db.collection('session').insert(session, 
            function (err, response) {
                cb(null, {
                    id: session._id
                });
            });
        },
        storeToken: function (data, cb) {
            data.db.collection('session').update({ _id: data.id }, {
                $set: {
                    refreshToken: data.refreshToken,
                    expiryTime: data.expiryTime
                }
            }, function (err, response) {
                cb(err);
            });
        },
        findToken: function (data, cb) {
            console.log('findToken: ' + JSON.stringify(data.body));

            if (!data.body.refreshToken) {
                return cb(new Error('invalid token'));
            }
            data.db.collection('session').findOne({ refreshToken: data.body.refreshToken }, function (err, response) {
                if (err) {
                    err.status = 401;
                    return cb(err);
                }

                if (!response) {
                    var err_1 = new Error('not found');
                    err_1.status = 401;
                    return cb(err_1);
                }

                var token = {
                    id: response.userId,
                    clientId: response._id,
                    rejected: response.rejected,
                    expired: moment().diff(response.expiryTime) > 0
                }

                console.log('refresh token: ' + JSON.stringify(token));

                cb(null, token);
            });
        },
        rejectToken: function (data, cb) {
            data.db.collection('session').updateOne(
                { refreshToken: data.body.refreshToken },
                { $set: { rejected : true } },
                function (err, response) {
                    if (err) {
                        return cb(err);
                    }

                    if (!response || response.matchedCount == 0) {
                        var err_1 = new Error('not found');
                        err_1.status = 401;
                        return cb(er_1);
                    }

                    cb(null, null);
            });

            
        }
    },

    helper: {
        getNextSequence: function(data, cb) {
            data.db.collection('counters').findOneAndUpdate(
                { _id: data.name },
                { $inc: { seq: 1 } },
                { upsert: true, returnOriginal: false },
                function (err, response) {
                    if (err) return cb(err);
                    cb(err, response.value.seq);
                }
            );
        }
    },

    company: function(data, cb) {
        data.db.collection('company').findOne({}, function(err, company) {
            cb(err, company);
        });
    }
};
//# sourceMappingURL=db.js.map