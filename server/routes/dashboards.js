var dashboards = {
	getHGResults : function(req, res, next) {
	    var db = req.db;
	    var type = req.params.type;

	    console.log('getHGResults');

	    db.collection('cobjects').aggregate([
	    		{ $match: { $and: [
	    						{ __type:'examination' },
	    						{ __recordtype: 'heartGuard' }
	    					]},
	    		},
	    		{ $group: { 
	    				_id: {hg_resultGreen : '$hg_resultGreen', hg_resultRed : '$hg_resultRed'}, 
	    				count: { $sum: 1 }
	    				} 
	    		}
	    	], function(err, result) {
	    		
	    		res.json(result);
	    	});
	    
	}
}

module.exports = dashboards;