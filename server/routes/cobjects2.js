var base64 = require('base64-stream');
var sts = require('string-to-stream');
var async = require("async");
var db = require('./db');

var cobjects = {
    getChildRecords : function(req, res, next) {
        var db = req.db;
        var parentId = req.query.parentId;
        var childField = req.query.childField;

        console.log('GET /childs');
        console.log('parentId:' + parentId + ' - childField:' + childField);

        var query = {};
        query[childField] = parentId;

        console.log('query:' + JSON.stringify(query));

        db.collection('cobjects').find(query,{'__file':false}).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    getLookups : function(req, res, next) {
        var db = req.db;
        var type = req.query.type;
        var what = req.query.what;
        var where = req.query.where;

        console.log('GET lookups');

        var query = {};
        query[where] = { $regex : what, '$options' : 'i'};
        // db.users.findOne({"username" : {$regex : ".*son.*"}});

        console.log('query:' + JSON.stringify(query));

        db.collection('cobjects').find({ $and: [ {__type:type}, query ]},{'__file':false}).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    getCObject : function(req, res, next) {
        var db = req.db;
        var objectId = req.params.id;
        var collection = req.query.collection;

        console.log('GET id:'  + objectId);

        db.collection(collection).find({__id:objectId},{'__file':false}).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    getField : function(req, res, next) {
        var db = req.db;
        var objectId = req.params.id;
        var field = req.params.field;

        var includeFilter = {};
        includeFilter[field] = true;
        includeFilter['__id'] = true;

        console.log('query:' + JSON.stringify(includeFilter));

        db.collection('cobjects').find({__id:objectId},includeFilter).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    getFile : function(req, res, next) {
        var db = req.db;
        var objectId = req.params.id;
        var fileField = req.params.fileField;
        var decode = req.query.decode;

        db.collection('cobjects').find({__id:objectId}).toArray(function(err, documents) {
            if (documents.length > 0) {
                var doc = documents[0];
                res.setHeader('content-type',doc.__fileType);
                var prefixLength = 'data:;base64,'.length + doc.__fileType.length;
                if (decode == '1') {
                    sts(doc[fileField].substring(prefixLength)).pipe(base64.decode()).pipe(res);
                }
                else {
                    sts(doc[fileField]).pipe(res);
                }
            }
            else {
                res.json();
            }
        });
    },

    getAllCObjects : function(req, res, next) {
        var db = req.db;
        var type = req.query.type;

        console.log('GET type:'  + type);
        
        db.collection('cobjects').find({__type:type},{'__file':false}).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    search : function(req, res, next) {
        var db = req.db;
        var searchTerm = req.query.searchTerm;

        db.collection('cobjects').createIndex({ "$**": "text" },{ name: "TextIndex" });
        db.collection('cobjects').find({ $text : { $search: searchTerm } }).toArray(function(err, documents) {
            res.json(documents);
        });
    },

    create : function(req, res, next) {
        var db = req.db;
        var body = req.body;
        console.log('body ' + JSON.stringify(body));
        var objectPrefix = req.query.prefix;

        
        db.collection('cobjects').insertOne(req.body, function(err, result) {
            console.log('err:' + err);
            console.log('result:' + result);
            console.log('id:' + result.insertedId);

            res.send(
                (err === null) ? { msg: 'successful', _id: result.insertedId, id: body.__id } : { msg: err }
            );
            
        });
    },

    update : function(req, res, next) {
        var db = req.db;
        var objectId = req.params.id;

        console.log('PUT id: ' + objectId + ' body: ' + JSON.stringify(req.body));
        delete req.body._id;
        db.collection('cobjects').replaceOne( 
            {__id:objectId},
            req.body, 
            function(err, result) {
                console.log(result);
                res.send({id : objectId, result: result});
            }
        )
    },

    delete : function(req, res, next) {
        var db = req.db;
        var id = req.params.id;
        var type = req.params.type;
        var collection = db.collection('cobjects');

        collection.find({__id:id}).limit(1).project({__recordtype:1}).toArray(function(err, documents) {
            var recordtype = documents[0].__recordtype;
            console.log('recordtype: ' + recordtype);
            var childObjects = new Array();

            db.collection('meta').find({type:type}).toArray(function(err, documents) {
                var meta = documents[0];
                
                meta.layouts.forEach(function(layout, index) {
                    console.log('layout.name: ' + layout.name);
                    if (layout.name == recordtype) {

                        if (layout.childs) {
                            layout.childs.forEach(function(child, index) {
                                childObjects.push(child);
                            });
                        }
                    }
                });

                
                
                collection.deleteOne({__id:id}, function(err, result) {
                    console.log('result: ' + result );
                    var resultJson = JSON.parse(result);
                    console.log('result.ok ' + resultJson.ok);
                    if (resultJson.ok) {
                        console.log('  childs: ' + JSON.stringify(req.body));

                        childObjects.forEach(function (item, index) {
                            console.log('child: ' + item);

                            var query = {};
                            query[item.refField] = id;    

                            var update = {};
                            update[item.refField] = '';

                            console.log('query: ' + JSON.stringify(query));
                            console.log('update: ' + JSON.stringify(update));

                            collection.updateMany(
                                query,
                                {
                                    $set: update
                                },
                                function(err, result){
                                    console.log('result: ' + result);
                                }
                            );
                        });

                    }
                    res.send(
                        (err === null) ? { result: result } : { msg: err }
                    );
                });

            });
        });
    },

    attachMetaData : function(req, res, next) {
        var db = req.db;
        var type = req.params.type;

        db.collection('meta').findOne({type : type}, function (err, meta) {
            if (err) next(err);
            if (!meta) next(new Error('meta data not found'));

            req.meta = meta;
            next();
        });
    },

    applyAutovalues : function(req, res, next) {
        if (req.meta) {
            var autovalueFields = req.meta.fields.filter(function (field) {
                if (field.type == 'autovalue') {
                    return true;
                }
            });
            async.each(autovalueFields, function(autovalueField, callback) {
                db.helper.getNextSequence({ name: autovalueField.name, db: req.db}, function (err, value) {
                    if (err) return callback(err);

                    req.body[autovalueField.name] = formatAutoValue(autovalueField.format, value);

                    callback();
                });
            }, function(err) {
                next(err);
            });
        }
    }
};

function formatAutoValue(format, value) {
    
    var indexLeftBracket = format.indexOf('{');
    var indexRightBracket = format.indexOf('}');
    var size = indexRightBracket - indexLeftBracket -1;
    var s = value + "";

    while (s.length < size) s = "0" + s;

    return format.substring(0, indexLeftBracket) + s + format.substr(indexRightBracket+1);
}

module.exports = cobjects;