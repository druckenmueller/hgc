'use strict';

//var jwt = require('jwt-simple');
var jwt = require('jsonwebtoken');

var crypto = require('crypto');

//var passport = require('passport');
//var LocalStrategy = require('passport-local');
var moment = require('moment');
var ObjectId = require('mongodb').ObjectID;
var db = require('./db');
var pwd = require('./pwdHash');

var SECRET = require('../config/secret.js');
var TOKENTIME = 60 * 60; // in seconds
var REFRESHTOKENTIME = TOKENTIME * 2;

var format = require('string-format')
var async = require("async");

//const bunyan = require('bunyan');
var nodemailer = require("nodemailer");
 
var auth = {
  
  respond : {
    auth: function (req, res) {
        res.status(200).json({
            user: req.user,
            token: req.token
        });
    },
    token: function (req, res) {
        res.status(201).json({
            token: req.token
        });
    },
    reject: function (req, res) {
        res.status(204).end();
    }
  },

  serializeClient: function(req, res, next) {
    db.client.updateOrCreate({
        user: req.user,
        db: req.db
    }, function (err, client) {
        if (err) {
            return next(err);
        }
        // we store information needed in token in req.user
        req.user.clientId = client.id;
        next();
    });
  },

  generateAccessToken: function(req, res, next) {
    req.token = req.token || {};
    req.token.accessToken = jwt.sign({
        id: req.user.id,
        clientId: req.user.clientId
    }, SECRET(), {
        expiresIn: TOKENTIME
    });
    next();
  },

  generateRefreshToken: function(req, res, next) {
    req.token.refreshToken = req.user.clientId.toString() + '.' + crypto.randomBytes(40).toString('hex');
    db.client.storeToken({
        id: req.user.clientId,
        refreshToken: req.token.refreshToken,
        expiryTime: moment().add(REFRESHTOKENTIME, 's').toISOString(),
        db: req.db
    }, function (err) {
        if (err) {
            return next(err);
        }
        next();
    });
  },

  validateRefreshToken: function(req, res, next) {
    console.log('validateRefreshToken: ' + JSON.stringify(req.body));
    db.client.findToken({
        body: req.body,
        db: req.db
    }, function (err, token) {
        if (err) {
            err.status = 401;
            return next(err);
        }
        if (token.expired || token.rejected) {
            var err_1 = new Error('token invalid');
            err_1.status = 401;
            return next(err_1);
        }
        req.user = token;
        next();
    });
  },

  rejectToken: function(req, res, next) {
    db.client.rejectToken({
      body: req.body,
      db: req.db
    }, 
    next);
  },
 

  /**
   * Change the password of an authenticated user. req.user must be set by authtentication function
   *
   * @param {!req}  Request
   *                body: { PasswordNew:aNewPassword }
   * @param {!res}  Response
   *                HTTP status 200 if successful
   *                HTTP status 406 if unsuccessful
   */
  changePassword: function(req, res, next) {
    db.user.find(req.db, { _id: new ObjectId(req.user.id)}, function(err, user) {
      if (err) return next(err);
      console.log('user found');

      req.db.collection('company').findOne({}, function(err, company) {
        if (err) return next(err);

        checkPasswordRules(req.body.passwordNew, company.passwordRules, function (checkResult) {
          if (checkResult.ok) {
            console.log('new password checked');

            pwd.crypto.hash(req.body.passwordNew, function(err, hash){
              if (err) return next(err);

              req.db.collection('user').updateOne(
                {_id: new ObjectId(req.user.id)}, 
                { $set: { 
                  password: hash, 
                  lastModifiedDate: new Date(), 
                  expiryDate: moment().add(company.passwordRules.expiresAfterDays,'d').toISOString() } 
                }, 
                function(err, pwdUpdate) {
                  if (err) return next(err);
                  
                  res.status(200).json(checkResult);
                });
            });
          }
          else {
            res.status(406).json(checkResult);
          }
        }); 
      });
        
    });
  },

  /**
   * Generate a temp password that is valid for a short period of time.
   * A link will be sent to the user via email.
   *
   * @param {!req}  Request
   *                body: { username:theUsername }
   * @param {!res}  Response
   *                HTTP status 200 if successful
   *                HTTP status 406 if user not found
   */
  forgotPassword: function(req, res, next) {
    async.waterfall([
      function(cb) {
        db.company({db : req.db},function(err, company) {
          cb(err, company);
        });
      },
      function(company, cb) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          cb(err, token, company);
        });
      },
      function(token, company, cb) {
        
        db.user.find(req.db, { username: req.body.username}, function(err, user) {
          if (!user) {
            res.status(406).json({
              msg: 'Kein Benutzer mit angegebener E-Mail Adresse gefunden.'
            });
          }
          else {
            db.user.update(req.db, user._id, {
              resetPasswordToken : token,
              resetPasswordExpires : moment().add(company.security.resetTokenExpiresInMinutes,'m').toISOString()
            }, function(err) {
              cb(err, token, company, user);
            });
          }
        });
      },
      function(token, company, user, done) {
        
        var smtpTransport = nodemailer.createTransport({
          service: company.email.smtp.service,
          auth: {
            user: company.email.smtp.user,
            pass: company.email.smtp.password
          },
          debug: true
        });
        var mailText = company.email.forgotPassword.text;
        var mailOptions = {
          to: user.email,
          from: company.email.forgotPassword.from,
          subject: company.email.forgotPassword.subject,
          text: format(mailText, req.headers.host, token)
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          console.log('mail err: ' + err);
          res.status(200).json({
            msg : format('Eine E-Mail wurde an {0} gesendet.',user.email)
          });
        });
      }
    ], function(err) {
      if (err) return next(err);
    });
  },

  /**
   * Reset the password as a subsequent call to /forgotPassword
   *
   * @param {!req}  Request
   *                params: token   the temp password generated by /forgotPassword 
   *                body: { password:aNewPassword }
   * @param {!res}  Response
   *                HTTP status 200 if successful
   *                HTTP status 406 if unsuccessful
   */
  resetPassword: function(req, res, next) {
    db.company({db : req.db},function(err, company) {
      if (err) return next(err);

      db.user.find(req.db, { resetPasswordToken: req.body.token }, function(err, user) {
        if (err) return next(err);

        if (user) {
          var diff = moment().diff(user.resetPasswordExpires);
          console.log('diff: ' + diff);
        }

        if (!user || moment().diff(user.resetPasswordExpires) > 0) {
          res.status(406).json({
            err : 'Password reset token is invalid or has expired.'
          });
        }
        else {
          checkPasswordRules(req.body.password, company.passwordRules, function (checkResult) {
            if (checkResult.ok) {
              pwd.crypto.hash(req.body.password, function(err, hash){
                if (err) return next(err);

                console.log('hash: ' + hash);
                console.log('user: ' + JSON.stringify(user));
                
                db.user.update(req.db, user._id, {
                  password : hash,
                  lastModifiedDate: new Date(),
                  expiryDate : moment().add(company.passwordRules.expiresAfterDays,'d').toISOString(),
                  resetPasswordToken : undefined,
                  resetPasswordExpires : undefined
                }, function(err) {
                  if (err) return next(err);

                  db.user.authenticate(req, user.username, req.body.password, function(err, authenticatedUser) {
                    if (err) return next(err);
                    req.user = authenticatedUser;
                    next();
                  });
                }); 
              });
            }
            else {
              res.status(406).json(checkResult);
            }
          });
        }
      });
    }); 
  } 
}


function checkPasswordRules(password, rules, cb) {
  let lengthOk = password.length >= rules.minLength;
  

  var result = {
        ok : lengthOk
  };

  if (!lengthOk) {
    result.msg = 'Das Passwort muss mindestens ' + rules.minLength + ' Zeichen lang sein.'
  }
  
  cb(result);
}

 
module.exports = auth;