import { hgcPage } from './app.po';

describe('hgc App', function() {
  let page: hgcPage;

  beforeEach(() => {
    page = new hgcPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
