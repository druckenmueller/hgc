var meta = {
	getOne : function(req, res, next) {
	    var db = req.db;
	    var type = req.params.type;

	    db.collection('meta').find({type:type}).toArray(function(err, documents) {
	        res.json(documents);
	    });
	},

	getMany : function(req, res, next) {
	    var db = req.db;
	    var types = req.params.types;

		var typeArr = types.split(',');

	    db.collection('meta').find({"type": {"$in": typeArr}}).toArray(function(err, documents) {
	        res.json(documents);
	    });
	},

	getAll : function(req, res, next) {
	    var db = req.db;
	    
	    db.collection('meta').find({}).toArray(function(err, documents) {
	        res.json(documents);
	    });
	},

	getAppConfig : function(req, res, next) {
		var db = req.db;

		db.collection('system').find({name:'appConfig'}).toArray(function(err, documents) {
	        res.json(documents);
	    });
	}
}

module.exports = meta;