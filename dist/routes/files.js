var files = {
	insertFile : function(req, res, next) {
		var fstream;
		req.pipe(req.busboy);
		req.busboy.on('file', function (fieldname, file, filename) {
			console.log('uploading: ' + filename);
			fstream = fs.createWriteStream(__dirname + '/files/' + filename);
			file.pipe(fstream);
			fstream.on('close', function() {
				res.send({file: 'ok'});
			});
		});
	}
};

module.exports = files;