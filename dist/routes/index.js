var express = require('express');
var router = express.Router();
var expressJwt = require('express-jwt');
var passport = require('passport');
var LocalStrategy = require('passport-local');
var async = require('async');

var auth = require('./auth.js');
var cobjects = require('./cobjects2.js');
var meta = require('./meta2.js');
var dashboard = require('./dashboards.js');
var db = require('./db');
var pwd = require('./pwdHash');

var SECRET = require('../config/secret.js');

passport.use(new LocalStrategy({ passReqToCallback: true }, function (req, username, password, done) {
    db.user.authenticate(req, username, password, done);
}));

var authenticate = expressJwt({
    secret: SECRET()
});

router.all('/api/v1/*', authenticate);
 
/*
 * Auth routes
 */
router.post('/auth', 
    passport.initialize(), 
    passport.authenticate('local', {
        session: false,
        scope: []
    }),
    auth.serializeClient, 
    auth.generateAccessToken, 
    auth.generateRefreshToken, 
    auth.respond.auth);

router.post('/token', 
    auth.validateRefreshToken,
    auth.generateAccessToken, 
    auth.generateRefreshToken, 
    auth.respond.token);


router.post('/token/reject', 
    auth.rejectToken, 
    auth.respond.reject);

router.post('/forgot', auth.forgotPassword);
router.post('/reset', 
    auth.resetPassword,
    auth.serializeClient, 
    auth.generateAccessToken, 
    auth.generateRefreshToken, 
    auth.respond.auth);
router.post('/api/v1/changePassword', auth.changePassword);

router.get('/scrypt/:key', function(req, res) {
    var key = req.params.key;
    console.log('key:'+key);
    var x = {};
    pwd.crypto.hash(key, function(err, hash) {
        console.log('hash:'+hash);
        x.hash = hash;

        pwd.crypto.verify(hash, key, function(err, result) {
            console.log('result:'+result);
            x.result = result;

            res.json(x);
        })
    });

    
});

/*
 * CObject routes
 */
router.get('/api/v1/cobjects/childs', cobjects.getChildRecords);
router.get('/api/v1/cobjects/lookups', cobjects.getLookups);
router.get('/api/v1/cobjects/search', cobjects.search);
router.get('/api/v1/cobjects/:id', cobjects.getCObject);
router.get('/api/v1/cobjects/:id/fields/:field', cobjects.getField);
router.get('/api/v1/cobjects/:id/files/:fileField', cobjects.getFile);
router.get('/api/v1/cobjects/', cobjects.getAllCObjects);
router.post('/api/v1/cobjects/:type',
    cobjects.attachMetaData,
    cobjects.applyAutovalues,
    cobjects.create);
router.put('/api/v1/cobjects/:id', cobjects.update);
router.delete('/api/v1/cobjects/:type/:id', cobjects.delete);

/*
 * Meta routes
 */
router.get('/api/v1/meta/cobject/:type', meta.getOne);
router.get('/api/v1/meta/cobjects/:types', meta.getMany);
router.get('/api/v1/meta/cobject', meta.getAll);
router.get('/api/v1/meta/appConfig', meta.getAppConfig);

/*
 * dashboard routes
 */
router.get('/api/v1/dashboards/hgresult', dashboard.getHGResults);
 
module.exports = router;