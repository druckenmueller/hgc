var scrypt = require('scrypt');
//var base64 = require('base-64');

module.exports = {
    crypto: {
        hash: function(key, cb) {
                /*
            var kdfResult = scrypt.kdfSync(key, scrypt.paramsSync(0.1));
            cb(null, kdfResult.toString('base64'));
            //cb(null, kdfResult);
*/

            scrypt.kdf(key, {N: 1, r:1, p:1}, function(err, result) {
                
            //scrypt.kdf(key, scrypt.paramsSync(0.1), function(err, result) {
                if (err) cb(err);

                cb(null, result.toString('base64'));
            });
            
        },

        verify: function(hash, key, cb) {
            var result = scrypt.verifyKdfSync(Buffer.from(hash, 'base64'), key);
            cb(null, result);
        }
    }
}