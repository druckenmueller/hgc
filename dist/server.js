var express = require('express'),
    path = require('path'),
    fs = require('fs'),
    nconf = require('nconf'),
    logger = require('morgan-body'),
    mongodb = require('mongodb'),
    bodyParser = require('body-parser');
//var busboy = require('connect-busboy');



var staticRoot = __dirname + '/';
console.log('staticRoot: ' + staticRoot);

nconf.argv()
   .env()
   .file({ file: staticRoot + '/config/config.json' });

nconf.defaults({
    'mongoURL': 'mongodb://localhost:27017/cardisio'
  });
/*
nconf.save(function (err) {
    fs.readFile(staticRoot + '/config/config.json', function (err, data) {
        console.log('err: ' + err);
      //console.dir(JSON.parse(data.toString()))
    });
}
);
*/


 
console.log(nconf.get());

const mongoURL = nconf.get('mongoURL');// 'mongodb://localhost:27017/cardisio';
const MongoClient = require('mongodb').MongoClient;

var app = express();


//app.set('port', (process.env.PORT || 3000));
app.set('port', nconf.get('port'));
//app.use(logger('dev'));

app.use(bodyParser.json({limit: '10mb'}));
//app.use(busboy());
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
logger(app);

var db;

//app.all('/api/v1/*', [require('./middlewares/validateRequest')]);

app.use(function(req, res, next) {
    console.log('added db to request: ' + db);
    req.db = db;
    next();
});

app.use('/', require('./routes'));


app.use(function(req, res, next){
    console.log('request: ' + req.path);

    // if the request is not html then move along
    var accept = req.accepts('html', 'json', 'xml');
    console.log('accept: ' + accept);
    if(accept !== 'html'){
        console.log('...calling next()')
        return next();
    }

    // if the request has a '.' assume that it's for a file, move along
    var ext = path.extname(req.path);
    if (ext !== ''){
        return next();
    }

    console.log('sending index.html');
    fs.createReadStream(staticRoot + 'index.html').pipe(res);

});

app.use(express.static(staticRoot));



MongoClient.connect(mongoURL, (err, database) => {
    if (err) return console.log(err);

    console.log("Connected to Database on " + mongoURL);
    
    db = database;
    app.listen(app.get('port'), function() {
        console.log('app running on port', app.get('port'));
    });

});
/*
app.listen(app.get('port'), function() {
    console.log('app running on port', app.get('port'));
});
*/