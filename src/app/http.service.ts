import { NgModule, Injectable } from '@angular/core';
import { Http, RequestOptions, Request, RequestOptionsArgs , Response, Headers, XHRBackend } from '@angular/http';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';
import { JwtHttp, JwtConfigService } from 'angular2-jwt-refresh';
import { Observable }     from 'rxjs/Observable';
import { AuthService } from './auth.service';
import './rxjs-operators';

@Injectable()
export class HttpService extends JwtHttp {
    constructor(refreshConfigService: JwtConfigService, http: Http, defOpts: RequestOptions, private authService: AuthService) {
        super(refreshConfigService, http, defOpts);
    }
    
    request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {  
        return super.request(url, options).catch((res: Response) => {
            return this.authService.checkForUnauthorizedResponse(res);
        });
  }
}