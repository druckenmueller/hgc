import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CO } from '../co';
import { COService } from '../COService';
import { MetaService } from '../meta.service';
import { FieldService } from '../FieldService';
import { Meta, FieldBase, Layout, LayoutSection, LayoutSlot, Recordtype } from '../meta';

@Component({
  template: `
  	<span *ngIf="meta">
	  	<form (ngSubmit)="onSubmit()" [formGroup]="form">
	      <p-toolbar>
	          <div class="ui-toolbar-group-right">
	            <button pButton type="submit" label="Speichern" [disabled]="!form.valid"></button>
	            <button pButton type="button" label="Abbrechen" (click)="onCancel()"></button>
	          </div>
	      </p-toolbar>
	      <p-fieldset *ngFor="let section of layout.sections" legend="{{section.name}}">
	        <div class="ui-g">
		        <div *ngFor="let slot of section.slots" class="ui-g-6">
		          <div *ngIf="slot.isBlank"></div>
		          <div *ngIf="!slot.isBlank">
		            <t-field [field]="meta.field[slot.name]" [cobject]="cobject" [isInput]="true" [form]="form" [showLabel]="true"></t-field>
		          </div>
		        </div>
	        </div>
	      </p-fieldset>
	    </form>
    </span>
  `
})
// <t-objectEdit *ngIf="id" [id]="id"></t-objectEdit>
export class EditComponent implements OnInit { 
	id: string;

	cobject: CO;
	meta: Meta;
	form: FormGroup;
	layout: Layout;
	
	constructor(
		private route: ActivatedRoute, 
		private router: Router,
		private service: COService,
    	private fieldService: FieldService,
    	private metaService: MetaService) {}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.id = params['id'];

			this.service.getObject(this.id).subscribe(
				cobject => {
					this.cobject = cobject;
					this.metaService.findMeta(this.cobject.type).subscribe(
						meta => {
							this.meta = meta;
    						this.layout = this.meta.getLayoutByRecordtypeName(this.cobject.recordtype);
    						this.form = this.fieldService.getFormGroupForEdit(this.meta, this.cobject);
						}
					);
				},
				error => console.log(error)
			);
		});
	}

	onCancel() {
	    this.router.navigate(['/view', this.cobject.id]);
	}

	onSubmit() {
	    this.meta.getFieldsByRecordtype(this.cobject.recordtype).forEach((f:FieldBase) => {
	    	if (!f.readonly && this.form.contains(f.name)) {
	        	this.cobject.setAttribute(f.name, f.formatValue(this.form.value[f.name]));
	      	}
	    });

	    this.service.updateCObject(this.cobject).subscribe(
	    	response => {
	    		console.log(response);
	    		this.router.navigate(['/view', this.cobject.id]);
	    	},
			error => console.log(error)
	    );
	    
	    // service.update(this.cobject);

	    
	}

}