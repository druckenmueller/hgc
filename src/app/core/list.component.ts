import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ConfirmDialogModule,ConfirmationService } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';

import { CO } from '../co';
import { COService } from '../COService';
import { Meta, FieldBase } from '../meta';
import { MetaService } from '../meta.service';
import { AuthService } from '../auth.service';

import * as moment from 'moment/moment';

@Component({
  template: `
  	<div *ngIf="cos">
   		<p-toolbar>
	        <div class="ui-toolbar-group-left">
	        	<i [ngClass]="meta.icon" class="fa fa-2x" aria-hidden="true"></i>
	         	<span>{{meta.labelPlural}}</span>
	        </div>
			<div class="ui-toolbar-group-left">
				<span style="padding-left:10px">
	        	<p-dropdown [options]="filter" [(ngModel)]="selectedFilter" (onChange)="onFilterChanged($event)"></p-dropdown>
				</span>
	        </div>
	            
	        <div class="ui-toolbar-group-right">
	         	<button pButton type="button" label="Neu" icon="fa-plus" (click)="onCreate()"></button>
	        </div>
      	</p-toolbar>
      	<p-dataTable [value]="filteredObjects" selectionMode="single" [(selection)]="selectedObject" (onRowSelect)="selectObject()">
   			<p-column header="Aktion" [style] = "{'width':'105px'}">
		        <template let-o="rowData" pTemplate type="body">
		        	<button type="button" pButton (click)="onEdit(o)" icon="fa-edit"></button>
              		<button type="button" pButton (click)="onDel(o)" icon="fa-trash"></button>
		        </template>
      		</p-column>
      		<p-column *ngFor="let field of fields" [header]="field.label" sortable="custom" (sortFunction)="mysort($event, field)">
      			<template let-o="rowData" pTemplate type="body">
		        	<t-field [field]="field" [cobject]="o" [isInput]="false" [showLabel]="false"></t-field>
		        </template>
      		</p-column>
   		</p-dataTable>
   	</div>
  `
})
export class ListComponent implements OnInit { 
	type: string;
	cos: CO[];
	filteredObjects: CO[];
	fields: FieldBase[];
	meta: Meta;
	selectedObject: CO;
	filter: SelectItem[];
	selectedFilter: any;
	selectedFilterByType: {[type:string] : any} = {};

	constructor(
		private route: ActivatedRoute, 
		private router: Router,
		private service: COService,
    	private metaService: MetaService,
    	private confirmationService: ConfirmationService,
		private authService: AuthService) {
			
			
			/*
			this.filter.push({label:'Alle anzeigen', value:{id:0, name: 'All', code: 'All'}});
        	this.filter.push({label:'Nur mein Datensätze', value:{id:1, name: 'MyRecords', code: 'MyRecords'}});
			this.filter.push({label:'Kürzlich geändert', value:{id:2, name: 'RecentRecords', code: 'RecentRecords'}});
			*/
			//this.selectedFilter = this.filter[0].value;
		}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.type = params['type'];
			
			this.loadObjects();
		});
	}


	loadObjects() {
		this.metaService.findMeta(this.type).subscribe(
			meta => {
				this.meta = meta;
				this.initFilter();
				this.service.getObjects(this.type).subscribe(
					cobjects => {
						this.cos = cobjects;
						this.applyFilter();
						this.fields = this.meta.listviewFields;
					},
					error => console.log(error)
				);
			},
			error => console.log(error)
		);
	}

	initFilter() {
		this.filter = [];
			
		this.filter.push({
			label:'Alle anzeigen', 
			value: { 
				name: 'all', 
				criteria: function (user, co, index, array) {
					return true;
				}
			}});
		this.filter.push({
			label:'Nur mein Datensätze', 
			value: { 
				name: 'my', 
				criteria: function (user, co, index, array) {
					return co.createdById == user.__id;
				}
			}});
		this.filter.push({
			label:'Kürzlich geändert', 
			value:{
				name: 'recent', 
				criteria: function (user, co, index, array) {
					return moment().diff(co.lastModifiedDate,'days') <= 14;
				}
			}});


		this.meta.filter.forEach((filter:any) => {
			let criteria = new Function('user', 'co', 'index', 'array', 'var o = co.raw; return ' + filter.criteria);
			this.filter.push({
				label : filter.label,
				value : {
					name : filter.name,
					criteria : criteria
				}
			});
		});

		this.selectedFilter = this.selectedFilterByType[this.type];

		if (!this.selectedFilter) {
			this.selectedFilter = this.filter[0].value;
		}
	}

	onCreate() {
		this.router.navigate(['/new', this.type]);
	}

	onEdit(cobject: CO) {
		console.log("edit: " + cobject.id);
		this.router.navigate(['/edit', cobject.id]);
	}

	onDel(cobject: CO) {
		this.confirmationService.confirm({
            message: 'Datensatz löschen?',
            accept: () => {
                this.service.deleteCObject(cobject, this.meta).subscribe((response) => {
					this.loadObjects();
				});
            }
        });

	}

	selectObject() {
		this.router.navigate(['/view', this.selectedObject.id]);
	}

	onFilterChanged(event) {
		this.selectedFilterByType[this.type] = this.selectedFilter;
		this.applyFilter();
	}

	private applyFilter() {
		this.filteredObjects = this.cos.filter(this.selectedFilter.criteria.bind(null, this.authService.user));

	}

	compare(field: FieldBase, direction: number) {
		return function(a:CO, b:CO): number {
			if ( a.getAttribute(field.name) < b.getAttribute(field.name) )
				return -1 * direction;
			if ( a.getAttribute(field.name) > b.getAttribute(field.name) )
				return 1 * direction;
			return 0;
		}
	}

	mysort(event, field: FieldBase) {
		this.filteredObjects.sort(this.compare(field, event.order));
	}
}