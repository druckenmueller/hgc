import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmDialogModule,ConfirmationService } from 'primeng/primeng';

import { CO } from '../co';
import { COService } from '../COService';
import { Meta, ChildObject, FieldBase } from '../meta';
import { MetaService } from '../meta.service';

@Component({
  selector: 't-childObjects',
   template: `
      <br/>
      <div *ngIf="meta">
        <p-toolbar>
            <div class="ui-toolbar-group-left">
                <i [ngClass]="meta.icon" class="fa fa-2x" aria-hidden="true"></i>
                <span>{{meta.raw.labelPlural}}</span>
            </div>
                
            <div class="ui-toolbar-group-right">
               <button pButton type="button" label="Neu" icon="fa-plus" (click)="onCreate()"></button>
            </div>
        </p-toolbar>
        <p-dataTable [value]="cos" selectionMode="single" [(selection)]="selectedObject" (onRowSelect)="selectObject()">
          <p-column header="Aktion" [style] = "{'width':'105px'}">
              <template let-o="rowData" pTemplate type="body">
                <button type="button" pButton (click)="onEdit(o)" icon="fa-edit"></button>
                <button type="button" pButton (click)="onDel(o)" icon="fa-trash"></button>
              </template>
            </p-column>
            <p-column *ngFor="let field of fields" [header]="field.label">
              <template let-o="rowData" pTemplate type="body">
                <t-field [field]="field" [cobject]="o" [isInput]="false" [showLabel]="false"></t-field>
              </template>
          </p-column>
        </p-dataTable>
      </div>
   `
})
export class ChildListComponent implements OnInit {
  @Input() child: ChildObject;
  @Input() parentId: string;

  cos: CO[];
  fields: FieldBase[];
  meta: Meta;
  selectedObject: CO;
  
  constructor(
    private service: COService,
    private metaService: MetaService,
    private router: Router,
    private confirmationService: ConfirmationService) {}


  ngOnInit() {
    console.log('ChildListComponent.ngOnInit');
    this.loadObjects();
  }

  loadObjects() {
    this.service.getChildObjects(this.child.name, this.child.refField, this.parentId).subscribe(
      cobjects => {
        this.cos = cobjects;
        
        this.metaService.findMeta(this.child.name).subscribe(
          meta => {
            this.meta = meta;
            this.fields = this.meta.getFieldsForNames(this.child.listviewFieldnames);
          },
          error => console.log(error)
        );
        
      },
      error => console.log(error)
    );
  }

  selectObject() {
    this.router.navigate(['/view', this.selectedObject.id]);
  }  

  onEdit(cobject: CO) {
    this.router.navigate(['/edit', cobject.id]);
  }

  onDel(cobject: CO) {
    this.confirmationService.confirm({
      message: 'Datensatz löschen?',
      accept: () => {
        this.service.deleteCObject(cobject, this.meta).subscribe((response) => {
          this.loadObjects();
        });
      }
    });
  }

  onCreate() {
    this.router.navigate(['/new/' + this.meta.type, {value_0: this.parentId, field_0: this.child.refField}]); 
  }
}