import { Component, Input, OnInit } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { CO  } from '../co';
import { FieldBase } from '../meta';
import { ValidationResult } from '../validation.service';

@Component({
  selector: 't-field',
   template: `
   
    <div><label *ngIf="showLabel" [attr.for]="field.name" [ngClass]="{'ui-required': field.required}">{{field.label}}</label></div>

   	<span *ngIf="showInput" [formGroup]="form">
  		<span [ngSwitch]="field.controlType">
  		  	<select [id]="field.key" *ngSwitchCase="'dropdown'"  [formControlName]="field.name">
  	      		<option *ngFor="let opt of field.options" [value]="opt.name">{{opt.label}}</option>
  	    	</select>

  	    	<t-reffield *ngSwitchCase="'ref'" [field]="field" [form]="form" [cobject]="cobject"></t-reffield>

          <p-checkbox *ngSwitchCase="'checkbox'" [formControlName]="field.name" [binary]="true"></p-checkbox>

          <span *ngSwitchCase="'percentage'">
            <table>
              <tr>
                <td><p-slider [formControlName]="field.name" [style]="{'width':'200px'}" animate="true"></p-slider></td>
                <td>{{form.value[field.name]}}</td>
              </tr>
            </table>
          </span>

          <t-date *ngSwitchCase="'date'" [form]="form" [field]="field" [cobject]="cobject" [isInput]="true"></t-date>
          
          <t-recordtype *ngSwitchCase="'recordtype'" [field]="field" [cobject]="cobject"></t-recordtype>

          <t-file-upload *ngSwitchCase="'file'" [multiple]="false" [form]="form" [field]="field" [cobject]="cobject"></t-file-upload>
  		
          <input *ngSwitchCase="'number'" [pTooltip]="field.helpText" [id]="field.key" [type]="field.type" [step]="field.step" [min]="field.min" [max]="field.max" [formControlName]="field.name">

          <input *ngSwitchDefault [id]="field.key" [type]="field.type" [formControlName]="field.name">

          
      </span>
   	</span>
   	<span *ngIf="!showInput">
   		<span [ngSwitch]="field.controlType">
   			<span *ngSwitchCase="'dropdown'">{{field.getOptionLabel(cobject.getAttribute(field.name))}}</span>

         <t-reffield *ngSwitchCase="'ref'" [field]="field" [cobject]="cobject" [isInput]="false"></t-reffield>

        <span *ngSwitchCase="'checkbox'">
          <i *ngIf="cobject.getAttribute(field.name)" class="fa fa-check-square" aria-hidden="true"></i>
          <i *ngIf="!cobject.getAttribute(field.name)" class="fa fa-square-o" aria-hidden="true"></i>
        </span>

        <span *ngSwitchCase="'number'">
          {{cobject.getAttribute(field.name)}}
          <span *ngIf="field.type == 'percentage'">%</span>
        </span>

        <t-recordtype *ngSwitchCase="'recordtype'" [field]="field" [cobject]="cobject"></t-recordtype>

        <t-file-upload *ngSwitchCase="'file'" [isInput]="false" [field]="field" [cobject]="cobject"></t-file-upload>

        <t-date *ngSwitchCase="'date'" [field]="field" [cobject]="cobject" [isInput]="false"></t-date>

        <span *ngSwitchDefault>{{cobject.getAttribute(field.name)}}</span>
   		</span>
   	</span>
   
   `
})
export class FieldComponent implements OnInit {
  @Input() field: FieldBase;
  @Input() cobject: CO;
  @Input() isInput = true;
  @Input() form: FormGroup;
  @Input() showLabel = true;
  @Input() validationResult: ValidationResult;

  showInput:boolean;

  ngOnInit() {
    console.log('FieldComponent ngOnInit field:' + (this.field ? this.field.name : ' field is undefined')  + ' cobject: ' + JSON.stringify(this.cobject));
    this.showInput = this.isInput && !this.field.readonly;
  }
}