import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';

import { CO } from '../co';
import { COService } from '../COService';
import { MetaService } from '../meta.service';
import { Meta, Layout, LayoutSection, LayoutSlot } from '../meta';

@Component({
  template: `
  	

  	<div *ngIf="co">
  		<p-toolbar>
  			<div class="ui-toolbar-group-left">
  				<div class="ui-object-name">{{meta.label}}</div>
  				<t-field [field]="meta.nameField" [cobject]="co" [isInput]="false" [showLabel]="false"></t-field>
  			</div>
	        <div class="ui-toolbar-group-right">
	          <button pButton type="button" label="Bearbeiten" icon="fa-pencil-square" (click)="onEdit()"></button>
	          <button pButton type="button" label="Löschen" icon="fa-trash" (click)="onDelete()"></button>
	        </div>
	        <!--
	        <div class="ui-toolbar-group-right">
	        	<button *ngIf="isOfflineAvailable" pButton type="button" [disabled]="true" label="offline verfügbar"></button>
	        	<button *ngIf="!isOfflineAvailable" pButton type="button" label="offline hinzufügen" icon="fa-pencil-square" (click)="onAddForOffline()"></button>
	        </div>
	        -->
	    </p-toolbar>
    	<p-fieldset  *ngFor="let section of layout.sections" legend="{{section.name}}">
    		<div class="ui-g">
	    		<div *ngFor="let slot of section.slots" class="ui-g-6">
	    			<div *ngIf="slot.isBlank"></div>
	    			<div *ngIf="!slot.isBlank">
	    				<t-field [field]="meta.field[slot.name]" [cobject]="co" [isInput]="false" [showLabel]="meta.field[slot.name].showLabel"></t-field>
	    			</div>
	    		</div>
    		</div>
    	</p-fieldset>

    	<t-childObjects *ngFor="let c of layout.childObjects" [child]="c" [parentId]="co.id"></t-childObjects>
    </div>
  `
})

export class ViewComponent implements OnInit { 
	id: string;

	co: CO;
	meta: Meta;
	layout: Layout;
	isOfflineAvailable: boolean;
	
	constructor(
		private service: COService,
		private metaService: MetaService,
		private router: Router,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService) {}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.meta = undefined;
			this.co = undefined;

			this.id = params['id'];

			let metaKey = this.metaService.extractMetaKey(this.id);

			this.metaService.findMetaByKey(metaKey).subscribe(
				meta => {
					this.meta = meta;

					this.service.getObject(this.id).subscribe(
						cobject => {
							let rt = cobject.recordtype;
							this.layout = this.meta.getLayoutByRecordtypeName(rt);
							this.isOfflineAvailable = this.service.isOfflineAvailable(cobject.id);

							this.co = cobject;
						},
						error => console.log(error)
					);
				},
				error => console.log(error)
			);


			/*
			this.service.getCO(this.id).subscribe(
				cobject => {
					this.co = cobject;
					this.metaService.findMeta(this.co.type).subscribe(
						meta => {
							this.meta = meta;
							let rt = this.co.recordtype;
							this.layout = this.meta.getLayoutByRecordtypeName(rt);
							this.isOfflineAvailable = this.service.isOfflineAvailable(this.co.id);
						},
						error => console.log(error)
					);
				},
				error => console.log(error)
			);
			*/
		});
	}

	onEdit() {
		this.router.navigate(['/edit', this.co.id]);
	}

	onDelete() {
		this.confirmationService.confirm({
            message: 'Datensatz löschen?',
            accept: () => {
                this.service.deleteCObject(this.co, this.meta).subscribe((response) => {
					this.router.navigate(['/list', this.co.type]);
				});
            }
        });
	}

	onAddForOffline() {
		this.service.makeOfflineAvailable(this.co);
		this.isOfflineAvailable = this.service.isOfflineAvailable(this.co.id);
	}

	
}