import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListComponent }        from './list.component';
import { ViewComponent }        from './view.component';
import { NewComponent }         from './new.component';
import { EditComponent }        from './edit.component';

const coreRoutes: Routes = [
      { path: 'list/:type',  component: ListComponent },
      { path: 'view/:id',  component: ViewComponent },
      { path: 'new/:type',  component: NewComponent },
      { path: 'edit/:id',  component: EditComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(coreRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CoreRoutingModule { }