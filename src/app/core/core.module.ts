import { NgModule }                             from '@angular/core';
import { BrowserModule }                        from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule }     from '@angular/forms';
import { HttpModule }                           from '@angular/http';

import { DataTableModule,SharedModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { Header } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { FieldsetModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { OverlayPanelModule } from 'primeng/primeng';
import { ToolbarModule } from 'primeng/primeng';
import { TabMenuModule } from 'primeng/primeng';
import { ToggleButtonModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { BlockUIModule } from 'primeng/primeng';

import { CoreRoutingModule }                    from './core-routing.module';

// the main components
import { ListComponent }                        from './list.component';
import { ViewComponent }                        from './view.component';
import { NewComponent }                         from './new.component';
import { EditComponent }                        from './edit.component';

import { FieldComponent }                       from './field.component';
import { ChildListComponent }                   from './childlist.component';

// UI Controls
import { RefFieldComponent }                    from '../controls/reffield.component';
import { RecordtypeComponent }                  from '../controls/recordtype.component';
import { DateComponent }                        from '../controls/date.component';
import { FileUploadComponent }                  from '../controls/fileupload';

// services
import { COService }                            from '../COService';
import { MetaService }                          from '../meta.service';
import { FieldService }                         from '../FieldService';
import { ValidationService }                    from '../validation.service';
import { AuthService }                          from '../auth.service';

import '../rxjs-operators';


@NgModule({
  imports:      [ 
    BrowserModule, 
    ReactiveFormsModule, 
    FormsModule, 
    HttpModule,

    DataTableModule,
    SharedModule,
    AutoCompleteModule,
    ButtonModule,
    InputTextModule,
    CheckboxModule,
    CalendarModule,
    FieldsetModule,
    SliderModule,
    PanelModule, 
    OverlayPanelModule,
    ToolbarModule,
    TabMenuModule,
    ToggleButtonModule,
    GrowlModule,
    BlockUIModule,

    CoreRoutingModule
    ],
  declarations: [ 
    ListComponent,
    ViewComponent, 
    NewComponent,
    EditComponent, 
    FieldComponent, 
    RefFieldComponent, 
    RecordtypeComponent,
    ChildListComponent,
    DateComponent,
    FileUploadComponent
  ],
  providers: [
      MetaService, COService, FieldService, ValidationService
  ]
})

export class CoreModule { }