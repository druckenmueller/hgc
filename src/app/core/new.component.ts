import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { CO } from '../co';
import { COService } from '../COService';
import { MetaService } from '../meta.service';
import { FieldService } from '../FieldService';
import { ValidationService, FieldValidator, ValidationResult } from '../validation.service';
import { Meta, FieldBase, Layout, LayoutSection, LayoutSlot, Recordtype } from '../meta';

@Component({
  template: `
  	<p-blockUI [blocked]="blocked"></p-blockUI>
  	<span *ngIf="meta">
	  	<div *ngIf="!recordtype">
	  		<p-toolbar>
				<div class="ui-toolbar-group-left">
					<div class="ui-object-name">{{meta.label}}</div>
					<div>Datensatztyp auswählen</div>
				</div>
	      	</p-toolbar>
	      	<div *ngFor="let rt of recordtypes" >
	      		<p><button pButton type="button" label="{{rt.label}}" (click)="onSelectRecordtype(rt)"></button></p>
				
	      	</div>
	  	</div>
	  	<form *ngIf="recordtype" (ngSubmit)="onSubmit()" [formGroup]="form">
	  		
	  		<p-toolbar>
				<div class="ui-toolbar-group-right">
					<button pButton type="submit" label="Speichern"></button>
					<button pButton type="button" label="Abbrechen" (click)="onCancel()"></button>
				</div>
	      	</p-toolbar>
	    	<p-fieldset *ngFor="let section of layout.sections" legend="{{section.name}}">
	    		<div class="ui-g">
		    		<div *ngFor="let slot of section.slots" class="ui-g-6">
		    			<div *ngIf="slot.isBlank"></div>
		    			<div *ngIf="!slot.isBlank">
		    				<t-field [field]="meta.field[slot.name]" 
		    					[cobject]="cobject" 
		    					[isInput]="true" 
		    					[form]="form" 
		    					[validationResult]="fieldValidator.result"
		    					[showLabel]="true"></t-field>
		    			</div>
		    		</div>
	    		</div>
	    	</p-fieldset>
	    	
	    </form>
    </span>
  `
})
// <button pButton type="submit" label="Save" [disabled]="!form.valid"></button>
// 
export class NewComponent implements OnInit { 
	type: string;
	meta: Meta;
	layout: Layout;
	recordtype: Recordtype;
	recordtypes: Recordtype[];
	form: FormGroup;
	cobject: CO;
	fieldValidator: FieldValidator;
	formErrors: {[key:string] : string} = {};
	blocked:boolean = false;
  	
	constructor(
		private route: ActivatedRoute, 
		private router: Router,
		private service: COService,
		private fieldService: FieldService,
		private metaService: MetaService,
		private validationService: ValidationService) {}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.type = params['type'];

			this.metaService.findMeta(this.type).subscribe(
				meta => {
					this.meta = meta;
					this.recordtypes = this.meta.recordtypes;

					this.cobject = new CO({},this.type);

					// check key value pairs passed by the url
					// field_x => the name of the field for value_x
					Object.keys(params).forEach((key:string) => {
						if (key.startsWith('field_')) {
							let fieldNo = key.split('_')[1];
							this.cobject.setAttribute(params[key], params['value_' + fieldNo]);
						}
					});

					if (this.recordtypes.length == 1) {
						this.onSelectRecordtype(this.recordtypes[0]);
					}
				},
				error => console.log(error)
			);
			
		});
	}

	onNewCObject(cobject : CO) {
		console.log('onNewCObject');
	}

	onSelectRecordtype(rt:Recordtype) {
		this.recordtype = rt;
		this.cobject.recordtype = this.recordtype.name;

		this.fieldValidator = this.validationService.getFieldValidator(
			this.meta.getFieldsByRecordtype(this.recordtype.name)
		);



		this.layout = this.meta.getLayoutByRecordtypeName(rt.name);
		this.form = this.fieldService.getFormGroupForEdit(this.meta, this.cobject);

	}

	onCancel() {
		this.router.navigate(['/list', this.type]);
	}


	onSubmitFile() {
		this.blocked = true;

		let reader:FileReader = new FileReader();
        let file:File = this.form.value['__file'];
        let co = new CO({}, this.type, this.recordtype.name, this.meta.key);

        var _this = this;
        reader.onload = function () {
			_this.form.controls['__file'].setValue(reader.result);
			console.log('converted');
			
			_this.meta.getFieldsByRecordtype(co.recordtype).forEach((f:FieldBase) => {
	    		if (_this.form.contains(f.name)) {
	        		co.setAttribute(f.name, f.formatValue(_this.form.value[f.name]));
	      		}
	    	});

	    	_this.blocked = false;

	    	_this.service.insertCObject(co).subscribe(
		    	response => {
		    		console.log(response);
		    		_this.router.navigate(['/view', co.id]);
		    	},
				error => console.log(error)
		    );
        };
        reader.onerror = function (error) {
          console.log('err: ' + error);
          _this.blocked = false;
        };

        reader.readAsDataURL(file);
        
	}

	onSubmit() {
		
		if (!this.form.valid) {
			this.fieldValidator.validate(this.form);
		}
		else {

			if (this.type == 'attachment') {
				this.onSubmitFile();
			}
			else {

				let co = new CO({}, this.type, this.recordtype.name, this.meta.key);
				co.meta = this.meta;

				this.meta.getFieldsByRecordtype(co.recordtype).forEach((f:FieldBase) => {
			    	if (!f.readonly && this.form.contains(f.name)) {
			        	co.setAttribute(f.name, f.formatValue(this.form.value[f.name]));
			      	}
			    });

			    this.service.insertCObject(co).subscribe(
			    	response => {
			    		console.log(response);
			    		this.router.navigate(['/view', co.id]);
			    	},
					error => console.log(error)
			    );
			}

    	}
    	
  	}
}