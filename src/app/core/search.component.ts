import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { CO } from '../co';
import { COService } from '../COService';
import { MetaService } from '../meta.service';
import { Meta, Layout, LayoutSection, LayoutSlot,ChildObject, FieldBase } from '../meta';

@Component({
  template: `
        <p-dataTable *ngFor="let type of types " [value]="objectsByType[type]" selectionMode="single" [(selection)]="selectedObject" (onRowSelect)="selectObject()">
      		<p-column *ngFor="let field of fieldsByType[type]" [header]="field.label">
      			<template let-o="rowData" pTemplate type="body">
		        	<t-field [field]="field" [cobject]="o" [isInput]="false" [showLabel]="false"></t-field>
		        </template>
      		</p-column>
   		</p-dataTable>
  `
  })

  

export class SearchComponent implements OnInit { 
    private types:string[] = [];
    private objectsByType: {[type:string]: CO[]} = {};
    private fieldsByType: {[type:string]: FieldBase[]} = {};
    
    selectedObject: CO;

    constructor(
		private service: COService,
		private metaService: MetaService,
		private router: Router,
		private route: ActivatedRoute) {}


    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let searchTerm:string = params['searchTerm'];

            if (searchTerm.length > 2) {
                this.search(searchTerm);
            }
        });
    }

    search(searchTerm: string) {
        this.service.doTextSearch(searchTerm).subscribe(
            cobjects => {
                
                cobjects.forEach((co:CO) => {
                    let cos:CO[] = this.objectsByType[co.type];

                    if (cos == undefined) {
                        this.objectsByType[co.type] = [];
                        cos = this.objectsByType[co.type];
                    }
                    cos.push(co);

                    if (this.fieldsByType[co.type] == undefined) {
                        this.fieldsByType[co.type] = co.meta.listviewFields;
                    }
                });

                for (let type in this.objectsByType) {
                    this.types.push(type);
                }
            }
        );
    }

    selectObject() {
		this.router.navigate(['/view', this.selectedObject.id]);
	}
}