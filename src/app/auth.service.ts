import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router }      from '@angular/router';
import { Config } from './config';

import { Observable } from 'rxjs/Observable';
import './rxjs-operators';

@Injectable()
export class AuthService {
	static readonly ACCESS_TOKEN = 'accessToken';
	static readonly REFRESH_TOKEN = 'refreshToken';

  isLoggedIn: boolean = false;
  user: any = {};

  private authUrl = Config.serverUrl + '/auth';  // URL to web API

  constructor(private http: Http, private router: Router) {}

  // store the URL so we can redirect after logging in
  redirectUrl: string;

	handleLoginResponse(response:any) : boolean {
		let loginInfo = response.json();

		if (response.ok) {
				
				this.user = loginInfo.user;
				sessionStorage.setItem(AuthService.ACCESS_TOKEN,loginInfo.token.accessToken);
				sessionStorage.setItem(AuthService.REFRESH_TOKEN,loginInfo.token.refreshToken);
				
				return true;
			}
			else {
				sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
				sessionStorage.removeItem(AuthService.REFRESH_TOKEN);
				
				
				return false;
			}
	}

  login(username: string, password: string): Observable<boolean> {
		// remove old tokens
		sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
		sessionStorage.removeItem(AuthService.REFRESH_TOKEN);

  	return this.http.post(
  				this.authUrl, 
  				{username: username, password:password}
  			).map((response:any) => {
					this.isLoggedIn = this.handleLoginResponse(response);
					return this.isLoggedIn;
				})
  			.catch((e:any) => {
  				this.isLoggedIn = false;
  				sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
					sessionStorage.removeItem(AuthService.REFRESH_TOKEN);
  				
					return Observable.of(false);
  			});

  }

  logout(): void {
    this.isLoggedIn = false;
  }

	checkForUnauthorizedResponse(res:Response) : Observable<Response> {
		if (res.status === 401) { 
			this.isLoggedIn = false;
			sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
			sessionStorage.removeItem(AuthService.REFRESH_TOKEN);
			this.router.navigate(['/login']);
		}

		return Observable.throw(res);
	}

	changePassword(passwordOld: string, passwordNew:string) : Observable<any> {
		let headers = new Headers();
		headers.append('Authorization', 'Bearer ' + sessionStorage.getItem(AuthService.ACCESS_TOKEN));

		return this.http.post(Config.serverUrl + '/api/v1/changePassword',
			{
				passwordOld: passwordOld,
				passwordNew: passwordNew
			},
			new RequestOptions({ headers: headers }))
			.map((response:Response) => {
				this.isLoggedIn = response.ok;
				return response.json();
			})

	}

	passwortForgotten(username: string) : Observable<any> {
		return this.http.post(Config.serverUrl + '/forgot', 
		{ 
			username: username 
		})
		.map((res:Response) => {
			return res.json();
		});
	}	

	resetPassword(password: string, token: string) : Observable<any> {
		return this.http.post(Config.serverUrl + '/reset',
		{
			password : password,
			token : token
		})
		.map((response:any) => {
			this.isLoggedIn = this.handleLoginResponse(response);
			return this.isLoggedIn;
		})
		.catch((e:any) => {
			this.isLoggedIn = false;
			sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
			sessionStorage.removeItem(AuthService.REFRESH_TOKEN);

			return Observable.throw(e);
		})
	}
}
