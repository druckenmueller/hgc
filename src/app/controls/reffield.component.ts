import { Component, Input, OnInit } from '@angular/core';
import { FormGroup }        from '@angular/forms';
import { Router } from '@angular/router';

import { OverlayPanel } from 'primeng/primeng';

import { CO } from '../co';
import { Meta, RefField } from '../meta';
import { COService } from '../COService';
import { MetaService } from '../meta.service';

@Component({
  selector: 't-reffield',
   template: `
     
    <span *ngIf="currentMeta"> 
     	<div *ngIf="showInput" [formGroup]="form">
        <p-overlayPanel #op>
          
          <p-dataTable [value]="targetOptions" selectionMode="single" [(selection)]="selectedTarget" (onRowSelect)="onTargetSelect(op, $event)" [style]="{'width':'500px'}">
            <p-column *ngFor="let fieldname of currentMeta.lookupviewFieldnames" [header]="currentMeta.field[fieldname].label">
              <template let-o="rowData" pTemplate type="body">
                <t-field [field]="currentMeta.field[fieldname]" [isInput]="false" [showLabel]="false" [cobject]="o"></t-field>
              </template>
            </p-column>
          </p-dataTable>
          
           
        </p-overlayPanel>

        <input type="hidden" [value]="currentTargetId" [formControlName]="field.name">
     		<input type="text" [value]="currentTargetValue" #target>
     		<button pButton type="button" (click)="onSearch(op, $event, target)" label="search"></button>
     	</div>
      <span *ngIf="!showInput">
        <span *ngIf="currentTargetCo">
        <t-field [field]="currentMeta.nameField" [isInput]="false" [showLabel]="false" [cobject]="currentTargetCo"></t-field>
        <button pButton type="button" icon="fa-external-link" (click)="onNavigateToTarget()"></button>
        </span>
      <span>
    </span>
   `
})
/*
<div *ngFor="let o of targetOptions">
          <div *ngFor="let fieldname of currentMeta.lookupviewFieldnames">
            {{fieldname}}
            <span>{{currentMeta[fieldname].name}}</span>
          </div>
        </div>
*/
export class RefFieldComponent implements OnInit {
  @Input() field: RefField;
  @Input() cobject: CO;
  @Input() form: FormGroup;
  @Input() isInput = true;
  
  currentTargetCo: CO;
  currentTargetId: string;
  currentTargetValue: string = '';
  currentMeta: Meta;
  targetOptions: CO[];
  selectedTarget: CO;
  showInput: boolean;

  constructor(
    private service: COService,
    private metaService: MetaService,
    private router: Router) {}

  onSearch(overlaypanel: OverlayPanel, event, target) {
  	let searchTerm = target.value;

    // ask service for target object hits
    this.service.searchObjects(searchTerm, this.field.refTo, this.currentMeta.lookupsearchfields).subscribe(
      cobjects => {
        this.targetOptions = cobjects;
        // show target list
        overlaypanel.toggle(event);
      },
      error => console.log(error)
    );
    //this.targetOptions = this.service.findObjects(searchTerm, this.field.refTo, this.currentMeta.lookupsearchfields);

  }

  ngOnInit() {
  	console.log('RefFieldComponent ngOnInit form:' + this.form + ' field:' + this.field);

    this.showInput = this.isInput && this.field.refTo != '';

    // object has already a ref value => lookup target object
    if (this.cobject.hasAttribute(this.field.name)) {
      this.currentTargetId = this.cobject.getAttribute(this.field.name);
      //TODO: load meta first
      this.service.getObject(this.currentTargetId).subscribe(
        cobject => {
          this.currentTargetCo = cobject;
          this.currentMeta = cobject.meta;
          this.currentTargetValue = this.currentTargetCo.getAttribute(this.currentMeta.nameField.name);
        },
        error => console.log(error)
      );
    }
    // new object with empty ref field and target object type known
    else if (this.field.refTo != '') {
      this.metaService.findMeta(this.field.refTo).subscribe(
        meta => {
          this.currentMeta = meta;
        },
        error => console.log(error)
      );
    }

    /*
    this.metaService.findMeta(this.field.refTo).subscribe(
      meta => {
        this.currentMeta = meta;

        if (this.cobject.hasAttribute(this.field.name)) {
          this.currentTargetId = this.cobject.getAttribute(this.field.name);

          this.service.getObject(this.currentTargetId).subscribe(
            cobject => {
              this.currentTargetCo = cobject;
              this.currentTargetValue = this.currentTargetCo.getAttribute(this.currentMeta.nameField.name);
            },
            error => console.log(error)
          );
        }
      },
      error => console.log(error)
    );
*/
    
    
  	
  }

  onTargetSelect(overlaypanel: OverlayPanel, event) {
    overlaypanel.hide();
    this.currentTargetCo = this.selectedTarget;
    this.currentTargetValue = this.currentTargetCo.getAttribute(this.currentMeta.nameField.name);
    this.form.controls[this.field.name].setValue(this.currentTargetCo.id);
  }

  onNavigateToTarget() {
    this.router.navigate(['/view', this.currentTargetCo.id]);
  }
}