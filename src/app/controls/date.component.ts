import { Component, Input, OnInit } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { CO } from '../co';
import { Meta, DateField, DateTimeField } from '../meta';
import { COService } from '../COService';

import * as moment from 'moment/moment';
import 'moment/locale/de';

@Component({
  selector: 't-date',
   template: `
      <span *ngIf="isInput" [formGroup]="form">
        <span *ngIf="cobject">
          <p-calendar 
            [placeholder]="cobject.getAttribute(field.name)" 
            [formControlName]="field.name" 
            [readonlyInput]="readonlyInput" 
            [showIcon]="true">
          </p-calendar>
        </span>
        <span *ngIf="!cobject">
          <p-calendar 
            [formControlName]="field.name" 
            [readonlyInput]="readonlyInput" 
            [showIcon]="true">
          </p-calendar>
        </span>
      </span>
      <span *ngIf="!isInput">
        <span>
          {{formattedDate}}
        </span>
      </span>

   `
})

export class DateComponent implements OnInit {
  @Input() field: DateField;
  @Input() cobject: CO;
  @Input() form: FormGroup;
  @Input() isInput: boolean = true;
  @Input() dateString: string;
  @Input() isDateTime: boolean = true;

  formattedDate:string = '';
  
  constructor() {}
  
  ngOnInit() {
    // default format
    let localeFormat: string = this.isDateTime ? 'L LTS' : 'L';
    let date:Date = new Date(this.dateString);

    // if cobject is set then override date and dateformat
    if (this.cobject) {
      moment.locale('de');
      localeFormat = this.field instanceof DateTimeField ? 'L LTS' : 'L';
      date = this.field.toTypedValues(this.cobject.getAttribute(this.field.name));
    }

    if (date) {
      this.formattedDate = moment(date).format(localeFormat);
    }
  }

}