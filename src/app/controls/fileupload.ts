import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { Meta, FileField } from '../meta';
import { CO } from '../co';
import { COService } from '../COService';


@Component({
  selector: 't-file-upload',
   template: `
    <span *ngIf="isInput">
      <input *ngIf="isInput" (click)="reset($event)" (change)="onChange($event)" type="file" #fileInput>
    </span>
    <span *ngIf="!isInput">
      <button pButton type="button" label="download" icon="fa-download" (click)="filedownload()"></button>
    </span>

    
   `
})
/*
<input type="file" (change)="fileChangeEvent($event)" placeholder="Upload file..." />

*/
export class FileUploadComponent implements OnInit  {
  @Input() field: FileField;
  @Input() form: FormGroup;
  @Input() cobject: CO;
  @Input() multiple: boolean = false;
  @Input() isInput: boolean = true;
  @ViewChild('fileInput') inputEl: ElementRef;

  fileBase64 : string;
  downloadLink: string;
  
  constructor(private el: ElementRef, private service: COService) {}

  ngOnInit() {
    console.log('init ');
    this.downloadLink = '/api/v1/cobjects/' + this.cobject.id + '/files/__file'
  }

  
  /*
  prepareUpload() {
    console.log('*** file change ');
    
      let inputEl: HTMLInputElement = this.inputEl.nativeElement;
      let fileCount: number = inputEl.files.length;
      
      if (fileCount > 0) { // a file was selected
        //this.field.file = inputEl.files.item[0];
      
        var reader = new FileReader();
        var _this = this;
        reader.onload = function () {
          _this.fileBase64 = reader.result;
          console.log('converted');
        };
        reader.onerror = function (error) {
          console.log('err: ' + error);
        };

        reader.readAsDataURL(inputEl.files.item[0]);
      }
      
  }
  */

  onChange(event: any) {
    console.log('onChange');
    let inputEl: HTMLInputElement = this.inputEl.nativeElement;
      let fileCount: number = inputEl.files.length;
      
      if (fileCount > 0) { // a file was selected
        //this.form.controls[this.field.name].setValue(inputEl.files.item(0));
        //this.field.file = inputEl.files.item(0);
      
        var file = inputEl.files.item(0);
        var reader = new FileReader();
        var _this = this;

        this.form.controls['__fileName'].setValue(file.name);
        this.form.controls['__fileSize'].setValue(file.size);
        this.form.controls['__fileType'].setValue(file.type);

        this.form.controls[_this.field.name].setValue(inputEl.files.item(0));
        /*
        reader.onload = function () {
          _this.form.controls[_this.field.name].setValue(reader.result);
          _this.fileBase64 = reader.result;
          console.log('converted');
        };
        reader.onerror = function (error) {
          console.log('err: ' + error);
        };

        reader.readAsDataURL(file);
        */
      }
  }

  reset(event: any) {
    //let inputEl: HTMLInputElement = this.inputEl.nativeElement;
    //inputEl.value = null;
  }

  /*
  onDownload() {
    
    this.service.getFile(this.cobject.id,'__file')
    .map(res => new Blob([res],{ type: this.cobject.getAttribute('__fileType') }))
        .subscribe(
          data => window.open(window.URL.createObjectURL(data)),
          error => console.log("Error downloading the file."),
                  () => console.log('Completed file download.'));

  }

  downloadfile(){
    var reader = new FileReader();
    this.service.getFile(this.cobject.id,'__file')
        .map(res => {
          console.log('before to blob');
          var b = new Blob([res],{ type: this.cobject.getAttribute('__fileType') })
          console.log('after to blob');
          return b;
        })
        .subscribe(res => reader.readAsDataURL(res), 
                    error => console.log("Error downloading the file."),
                    () => console.log('Completed file download.'));

    reader.onloadend = function (e) {
        window.open(reader.result, 'Excel', 'width=20,height=10,toolbar=0,menubar=0,scrollbars=no');
    }


  }
  */

  filedownload() {
    var req = new XMLHttpRequest();
    req.open("GET", this.downloadLink + '?decode=1', true);
    req.responseType = "blob";
    //req.setRequestHeader("x-auth", "secret"); TODO add token
    var _this = this;
    req.onload = function (event) {
        var blob = req.response;
        console.log(blob.size);
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download=_this.cobject.getAttribute('__fileName');
        link.click();
    };
    req.send();
  }

  

  
}