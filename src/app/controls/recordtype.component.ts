import { Component, Input, OnInit } from '@angular/core';

import { CO } from '../co';
import { Meta, RecordtypeField } from '../meta';
import { COService } from '../COService';

@Component({
  selector: 't-recordtype',
   template: `
    <span>{{field.recordtype[recordtypeName].label}}<span>
   `
})
/*
<input type="text" [value]="field.refValue" [formControlName]="field.name">
*/
export class RecordtypeComponent implements OnInit {
  @Input() field: RecordtypeField;
  @Input() cobject: CO;
  
  recordtypeName: string;

  constructor(
    private service: COService) {}

  
  ngOnInit() {
  	this.recordtypeName = this.cobject.getAttribute(this.field.name);
  }

 
}