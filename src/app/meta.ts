import * as moment from 'moment/moment';


export class Meta {
	private raw:any;
	field: {[fieldName:string]:FieldBase} = {};
	nameField: FieldBase;
	layout : {[layoutName:string]:Layout} = {};
	recordtype : {[recordtypeName:string]:Recordtype} = {};
	
	constructor(raw:any) {
		this.raw = raw;
		raw.layouts.forEach((l:any) => {
			this.layout[l.name] = new Layout(l);
		});

		raw.recordtypes.forEach((rt:any) => {
			this.recordtype[rt.name] = new Recordtype(rt);
		});

		raw.fields.forEach((f:any) => {
			if (f.type == 'text') {
				this.field[f.name] = new TextboxField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText})
			}
			else if (f.type == 'number') {
				this.field[f.name] = new NumberField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, 
						min:f.min, max:f.max, 
						scale: f.scale, type: 'number'});	
					
			}
			else if (f.type == 'autovalue') {
				this.field[f.name] = new AutovalueField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, format : f.format}
				);
			}
			else if (f.type == 'percentage') {
				this.field[f.name] = new NumberField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, min:f.min, max:f.max, type: 'percentage'});	
			}
			else if (f.type == 'email') {
				this.field[f.name] = new TextboxField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, type: 'email'})
			}
			else if (f.type == 'dropdown') {
				this.field[f.name] = new DropdownField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, options : f.options})
			}
			else if (f.type == 'ref') {
				this.field[f.name] = new RefField({
						name:f.name, 
						label: f.label, 
						required: f.required,
						readonly: f.readonly, 
						helpText: f.helpText,
						refTo : f.refTo, 
						searchFields: f.searchFields})
			}
			else if (f.type == 'boolean') {
				this.field[f.name] = new CheckboxField({name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText,isDefaultEnabled:f.isDefaultEnabled});
			}
			else if (f.type == 'date') {
				this.field[f.name] = new DateField({name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText});
			}
			else if (f.type == 'datetime') {
				this.field[f.name] = new DateTimeField({name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText});
			}
			else if (f.type == 'recordtype') {
				this.field[f.name] = new RecordtypeField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, recordtypes: this.recordtype});
			}
			else if (f.type == 'file') {
				this.field[f.name] = new FileField(
					{name:f.name, label: f.label, required: f.required, readonly: f.readonly, helpText: f.helpText, showLabel: f.showLabel});
			}
			else if (f.type == 'formula') {
				this.field[f.name] = new FormulaField(
					{name:f.name, label: f.label, readonly: true, helpText: f.helpText, showLabel: f.showLabel, formula: f.formula, formulaType: f.formulaType, scale: f.scale});
			}


			if (f.isNameField) {
				this.nameField = this.field[f.name];
			}
		});
	}

	get toJSON():any {
  		return this.raw;
  	}

	get type():string {
		return this.raw['type'];
	}

	get collection():string {
		return this.raw['collection'] || 'cobjects';
	}

	get key():string {
		return this.raw['key'];
	}

	get label():string {
		return this.raw['label'];
	}

	get labelPlural():string {
		return this.raw['labelPlural'];
	}	

	get icon():string {
		return this.raw['icon'];
	}

	get fields():any[] {
		return this.raw['fields'];
	}

	get listviewFieldnames():string[] {
		return this.raw['listview'];	
	}

	get lookupviewFieldnames():string[] {
		return this.raw['lookupview'];	
	} 

	get lookupsearchfields():string[] {
		return this.raw['lookupsearchfields'];
	}

	get recordtypes():Recordtype[] {
		let rts:Recordtype[] = [];

		this.raw['recordtypes'].forEach((rt:any) => {
			rts.push(new Recordtype(rt));
		});

		return rts;
	}

	get layouts():any[] {
		return this.raw['layouts'];	
	}	

	get listviewFields():FieldBase[] {
		return this.fields.filter((f:any) => 
			this.listviewFieldnames.some((fieldName:string) => 
				fieldName == f.name))
			.map((f:any) => {
				return this.field[f.name];
			});
	}	

	get formulaFields():FormulaField[] {
		return this.fields.filter((f:any) => f.type == 'formula');
	}

	get filter():any[]
 	{
	 	return this.raw['filter'] || [];
 	}
	getFieldsForNames(fieldNames: string[]) {
		return fieldNames.map((fieldName:string) => {return this.field[fieldName]});
	}

	getLayoutByRecordtypeName(recordtypeName: string):Layout {
		return this.layout[this.recordtype[recordtypeName].layout];
	}

	getFieldsByRecordtype(recordtypeName: string):FieldBase[] {
		let rtFields:FieldBase[] = [];

		this.getLayoutByRecordtypeName(recordtypeName).sections.forEach((ls:LayoutSection) => {
			ls.slots.forEach((s:LayoutSlot) => {
				if (!s.isBlank) {
					rtFields.push(this.field[s.name]);	
				}
			})
		});

		return rtFields;
	}

	
}

export class Layout {
	_name: string;
	_sections: LayoutSection[] = [];
	_childObjects: ChildObject[] = [];

	constructor(layoutJson: any) {
		this._name = layoutJson;
		layoutJson.sections.forEach((s:any) => {
			this._sections.push(new LayoutSection(s));
		});

		if (layoutJson.childs) {
			layoutJson.childs.forEach((c:any) => {
				this._childObjects.push(new ChildObject(c));
			});
		}
	}
	
	get sections(): LayoutSection[] {
		return this._sections;
	} 

	get name():string {
		return this._name;
	}

	get childObjects(): ChildObject[] {
		return this._childObjects;
	}
	
}


export class LayoutSection {
	_name: string;
	_slots: LayoutSlot[] = []; 

	constructor(layoutSectionJson: any) {
		this._name = layoutSectionJson.name;
		layoutSectionJson.slots.forEach((slotName:string) => {
			this._slots.push(new LayoutSlot(slotName));
		});
	}

	get slots(): LayoutSlot[] {
		return this._slots;
	} 

	get name():string {
		return this._name;
	}
}

export class LayoutSlot {
	_isBlank: boolean;
	_name: string;

	constructor(name: string) {
		this._name = name;
		this._isBlank = name === 'blank';
	}

	get isBlank():boolean {
		return this._isBlank;
	}

	get name():string {
		return this._name;
	}
}

export class Recordtype {
	_name: string;
	_layout: string;
	_label: string;

	constructor(rtJson: any) {
		this._name = rtJson.name;
		this._layout = rtJson.layout;
		this._label = rtJson.label;
	}

	get name():string {
		return this._name;
	}

	get layout():string {
		return this._layout;
	}

	get label():string {
		return this._label;
	}
}

export class ChildObject {
	_raw: any;
	_name: string;
	_refField: string;
	_listviewFieldnames: string[] = [];

	constructor(childJson: any) {
		this._raw = childJson;
		this._name = childJson.name;
		this._refField = childJson.refField;
		this._listviewFieldnames = childJson.listview;
	}

	get name():string {
		return this._name;
	}

	get refField():string {
		return this._refField;
	}

	get listviewFieldnames():string[] {
		return this._listviewFieldnames;
	}
}

export class FieldBase {
	name: string; 
	value: string; 
	label: string;
	controlType: string;
	required: boolean;
	readonly: boolean;
	showLabel: boolean = true;
	helpText: string;

	constructor(options: {
			name?: string,
			value?: string,
			label?: string,
			controlType?: string,
			required?: boolean,
			readonly? : boolean,
			showLabel?: boolean,
			helpText?: string
		} = {}) {

		this.name = options.name;
		this.value = options.value;
		this.label = options.label;
		this.helpText = options.helpText;
		this.controlType = options.controlType;
		this.required = options.required;
		this.readonly = this.readonly || options.readonly;
		this.showLabel = this.showLabel || options.showLabel;
	}

	toTypedValues(v: string): any {
		return v;
	}

	formatValue(v:any): any {
		return v;
	}

	validate(v: any) : string {
		return null;
	}

	resolveBeforeSave(callback) {
		callback();
	}

	
}

export class TextboxField extends FieldBase {
  controlType = 'textbox';
  type: string;
  
  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}

export class AutovalueField extends FieldBase {
  controlType = 'textbox';
  format: string;
  
  constructor(options: {} = {}) {
    super(options);
    this.format = options['format'] || '';
  }
}

export class RecordtypeField extends FieldBase {
  controlType = 'recordtype';
  recordtype : {[recordtypeName:string]:Recordtype} = {};
  
  constructor(options: {} = {}) {
    super(options);
    this.recordtype = options['recordtypes'];
  }
}

export class NumberField extends FieldBase {
  controlType = 'number';
  type: string;
  min: number;
  max: number;
  step: number;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.min = options['min'] || 0;
    this.max = options['max'] || 100;
	let scale = options['scale'];
	this.step = 1 / Math.pow(10, scale);
  }
}

export class CheckboxField extends FieldBase {
	controlType = 'checkbox';
	isDefaultEnabled: boolean;

	constructor(options: {} = {}) {
    super(options);
    this.isDefaultEnabled = options['isDefaultEnabled'] || false;
  }
}

export class DateField extends FieldBase {
	controlType = 'date';
	defaultDate: string;
	protected ISOFORMAT:string = 'YYYY-MM-DD';

	constructor(options: {} = {}) {
    	super(options);
    	this.defaultDate = options['defaultDate'] || '';
  	}

  	toTypedValues(v: string): any {
		return v == '' ? undefined : new Date(v);
	}

	formatValue(v: any): any {
		return v == '' ? '' : moment(v).format(this.ISOFORMAT);
	}
}

export class DateTimeField extends DateField {
	
	protected ISOFORMAT = 'YYYY-MM-DDThh:mm:ss';

}

export class DropdownField extends FieldBase {
	controlType = 'dropdown';
	options: {name: string, label: string, default: boolean}[] = [];

	constructor(options: {} = {}) {
		super(options);
		this.options = options['options'] || [];
	}

	getOptionLabel(optionName:string):string {
		let option = this.options.find((option:any) => option.name == optionName);
		return option ? option.label : '';
	}

	// return the default if no value provided => create new record
	toTypedValues(v: string): any {
		return v == '' ? this.options.find(o => o.default).name : v;
	}
}

export class RefField extends FieldBase {
	controlType = 'ref';
	//readonly = true;
	refTo: string;
	refValue: string = '';
	searchFields: string[];
	showFields: string[];


	constructor(options: {} = {}) {
   		super(options);
    	this.refTo = options['refTo'] || '';
    	this.searchFields = options['searchFields'] || [];
    	this.showFields = options['showFields'] || [];
  	}
}

export class FileField extends FieldBase {
	controlType = 'file';
	type = 'field';
	file: File;
}

export class FormulaField extends FieldBase {
	controlType = 'formula';
	formula: string;
	formulaType : string;
	scale : number;

	constructor(options: {} = {}) {
   		super(options);
   		this.formula = options['formula'] || '';
		this.formulaType = options['formulaType'] || 'text';
		this.scale = options['scale'] || 0;
   	}

	formatValue(v: any): any {
		if (this.formulaType == 'number') {
			return v.toFixed(this.scale);
		}
		return v;
	}
}
