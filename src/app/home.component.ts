import { Component, OnInit } from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import { DashboardService } from './dashboard.service';
import { AuthService }      from './auth.service';

@Component({
  template: `
    
    <p-chart *ngIf="data" type="pie" [data]="data" [options]="options" [width]="300"></p-chart>
    
  `
})
export class HomeComponent implements OnInit { 
	data: any;
    options: any;

	constructor(
        private dashboardService: DashboardService,
        private authService: AuthService
    ) {}

    

	ngOnInit() {
        this.dashboardService.getHeatGuardResultReport().subscribe(
            report => {
                let values = [];
                let labels = [];
                report.forEach(elem => {
                    values.push(elem.count);
                    labels.push(elem._id.hg_resultGreen ? 'grün [' + elem.count + ']' : elem._id.hg_resultRed ? 'rot [' + elem.count + ']' : 'kein Ergebnis [' + elem.count + ']');
                })
                
                this.data = {
                    labels: labels,
                    datasets: [
                        {
                            data: values,
                            backgroundColor: [
                                "#FF6384",
                                "#4BC0C0",
                                "#FFCE56"
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#4BC0C0",
                                "#FFCE56"
                            ]
                        }
                    ]    
                };

                this.options = {
                    title: {
                        display: true,
                        text: 'Heart Guard Ergebnisse',
                        fontSize: 16
                    },
                    responsive: false,
                    maintainAspectRatio: false
                };
            }
        );

		
	}
}
