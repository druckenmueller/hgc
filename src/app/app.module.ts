import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule }          from '@angular/forms';
import { HttpModule, JsonpModule, XHRBackend, RequestOptions } from '@angular/http';

//import { CoreModule } from './core/core.module';
import { AuthModule } from './auth.module';

// the main components
import { ListComponent }                        from './core/list.component';
import { ViewComponent }                        from './core/view.component';
import { NewComponent }                         from './core/new.component';
import { EditComponent }                        from './core/edit.component';
import { SearchComponent }                      from './core/search.component';

import { FieldComponent }                       from './core/field.component';
import { ChildListComponent }                   from './core/childlist.component';

// UI Controls
import { RefFieldComponent }                    from './controls/reffield.component';
import { RecordtypeComponent }                  from './controls/recordtype.component';
import { DateComponent }                        from './controls/date.component';
import { FileUploadComponent }                  from './controls/fileupload';


import { AppComponent }   from './app.component';
import { AppRoutingModule }  from './app-routing.module';
import { LoginRoutingModule }      from './login-routing.module';
import { LoginComponent }                       from './login.component';
import { ChangePasswordComponent }              from './changePassword.component';
import { ResetPasswordComponent }              from './resetPassword.component';
import { ContainerComponent } from './container.component';

import { DataTableModule,SharedModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { Header } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { FieldsetModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { OverlayPanelModule } from 'primeng/primeng';
import { ToolbarModule } from 'primeng/primeng';
import { TabMenuModule } from 'primeng/primeng';
import { ToggleButtonModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { BlockUIModule } from 'primeng/primeng';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';

import { HomeComponent } from './home.component';

// Services
import { COService } from './COService';
import { MetaService } from './meta.service';
import { FieldService } from './FieldService';
import { OfflineService } from './offline.service';
import { ValidationService } from './validation.service';
import { AuthService }              from './auth.service';
import { DashboardService } from './dashboard.service';
import { HttpService } from './http.service';

import './rxjs-operators';


@NgModule({
  imports:      [ 
    BrowserModule, 
    ReactiveFormsModule, 
    FormsModule,
    HttpModule,

    DataTableModule,
    SharedModule,
    AutoCompleteModule,
    ButtonModule,
    InputTextModule,
    CheckboxModule,
    CalendarModule,
    FieldsetModule,
    SliderModule,
    PanelModule, 
    OverlayPanelModule,
    ToolbarModule,
    TabMenuModule,
    ToggleButtonModule,
    GrowlModule,
    BlockUIModule,
    ConfirmDialogModule,
    ChartModule,
    DropdownModule,
    TooltipModule,
    MessagesModule,

    /*CoreModule, */
    AppRoutingModule, 
    AuthModule,
    LoginRoutingModule
    
    ],
  declarations: [ 
    AppComponent, 
    ContainerComponent,
    HomeComponent, 
    LoginComponent,
    ChangePasswordComponent,
    ResetPasswordComponent,
    ListComponent,
    ViewComponent, 
    NewComponent,
    EditComponent, 
    SearchComponent,
    FieldComponent, 
    RefFieldComponent, 
    RecordtypeComponent,
    ChildListComponent,
    DateComponent,
    FileUploadComponent
  ],
  providers: [
    /*
    {
      provide: HttpService,
      useFactory: (backend: XHRBackend, options: RequestOptions, authService: AuthService) => {
        return new HttpService(backend, options, authService);
      },
      deps: [XHRBackend, RequestOptions, AuthService]
    },
    */
    COService, 
    FieldService, 
    MetaService, 
    OfflineService, 
    ValidationService, 
    AuthService, 
    ConfirmationService, 
    DashboardService],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }