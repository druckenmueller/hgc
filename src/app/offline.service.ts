import { Injectable, Output, EventEmitter }       from '@angular/core';

import { CO } from './co';
import { Meta } from './meta';


@Injectable()
export class OfflineService {
	@Output() onlineStateChanged = new EventEmitter<boolean>();

	private isOnline:boolean = true;
	private metaItemCount:number = 0;

	constructor() {
	
	}

	get online():boolean {
		return this.isOnline;
	}

	setOnlineOffline(isOnline: boolean) {
		console.log('set online: ' + isOnline);
		if (this.isOnline != isOnline) {
			//this.onlineStateChanged.emit(isOnline);
		}

		this.isOnline = isOnline;
	}

	setObject(cobject: CO) {
		localStorage.setItem(cobject.id, JSON.stringify(cobject.toJSON));
	}

	setSaved(id:string) {
		localStorage.removeItem(id);
	}

	hasObject(id:string):boolean {
		return localStorage.getItem(id) != undefined;
	}

	getObject(id:string):CO {
		return new CO(JSON.parse(localStorage.getItem(id)));
	}

	getMetaObject(type: string): Meta {
		return new Meta(JSON.parse(localStorage.getItem('meta_' + type)));
	}

	getAllObjects():CO[] {
		let values: CO[] = [];

		for ( var i = 0, len = localStorage.length; i < len; ++i ) {
			let key:string = localStorage.key(i);

			//if (!key.startsWith('meta_')) {
			if (key.indexOf('meta_') == -1) {
				let o:any = JSON.parse(localStorage.getItem( key ) );
 				values.push(new CO(o));
 			}
		}

		return values;
	}

	getRecordCount():number {
		return localStorage.length - this.metaItemCount;
	}

	getCOs(type:string):CO[] {
		let values: CO[] = [];

		this.getAllObjects().forEach((co:CO) => {
			if (co.type == type) {
 				values.push(co);
 			}
		})

		return values;
	}

	findObjects(searchTerm: string, objectType: string, searchFields: string[]): CO[] {
		let values: CO[] = [];

		this.getAllObjects().forEach((co:CO) => {
			if (co.getAttribute(searchFields[0]).toLowerCase().indexOf(searchTerm) != -1 ) {
				values.push(co);
			}
		});

		return values;
	}

	getChildObjects(parentFieldName: string, parentId: string): CO[] {
		let values: CO[] = [];

		this.getAllObjects().forEach((co:CO) => {
			if (co.getAttribute(parentFieldName) === parentId) {
 				values.push(co);
 			}
		});


		return values;
	}

	deleteObject(co: CO, childObjectDefinition: any): any[] {
		return [];
	}

	cacheMetadata(metas: Meta[]) {
		console.log('adding meta data to cache...');
		metas.forEach((meta: Meta) => {
			localStorage.setItem('meta_' + meta.type, JSON.stringify(meta.toJSON));
			console.log('... added ' + meta.type);
		});
		this.metaItemCount = metas.length;
	}


}