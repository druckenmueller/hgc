import { Injectable }       from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { CO } from './co';
import { Meta, FieldBase, TextboxField, DropdownField, Recordtype } from './meta'

@Injectable()
export class FieldService {
	

    getFormGroupForEdit(meta: Meta, cobject: CO) {
    	let group: any = {};
    	let fieldvalidators:any[];

		meta.getFieldsByRecordtype(cobject.recordtype).forEach((field:FieldBase) => {
			fieldvalidators = [];

			if (field.required) {
				fieldvalidators.push(Validators.required);
			}

			group[field.name] = new FormControl(field.toTypedValues(cobject.getAttribute(field.name)) || '', fieldvalidators);
			/*
	    	group[field.name] = field.required 
	    		? new FormControl(field.toTypedValues(cobject.getAttribute(field.name)) || '', Validators.required)
	            : new FormControl(field.toTypedValues(cobject.getAttribute(field.name)) || '');
			*/
	    });



	    return new FormGroup(group);
    }

}

export function fieldValidator(field: FieldBase): ValidatorFn {
	return (control: AbstractControl): {[key: string]: any} => {
		const value = control.value;
		const validationResult = field.validate(value);
		
		if (validationResult) {
			let result = {};
			result[field.name] = validationResult;
			return result;
		}
		return null;
	};
}