import { Injectable,Output, EventEmitter  }       from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
//import { AuthHttp } from 'angular2-jwt';
import { JwtHttp } from 'angular2-jwt-refresh';
import { HttpService } from './http.service';

import { Config } from './config';

import { Observable }     from 'rxjs/Observable';
 

@Injectable()
export class DashboardService {
    private url = Config.serverUrl + '/api/v1/';

    constructor (
        private http: HttpService) {}

    getHeatGuardResultReport() : Observable<any> {
        return this.http.get(this.url + 'dashboards/hgresult')
          .map(response => response.json())
    }
}