import { Component, OnInit }   from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  template: `
    <p-messages [value]="msgs"></p-messages>
    <div *ngIf="!authService.isLoggedIn">
      <p-fieldset legend="Login">
    
      <!--<span *ngIf="loginFailed" style="color:red">Benutzer oder Passwort ungültig.</span>-->

      <div class="ui-g">
        <span class="ui-g-12"><label>Benutzername</label></span>
        <span class="ui-g-12"><input [type]="text" [(ngModel)]="username" required></span>
        <span class="ui-g-12"><label>Passwort</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="password" required></span>
      </div>
      
      <button pButton type="button" label="Login" (click)="login()"></button>
      <button pButton type="button" label="Passwort vergessen" (click)="forgotten()"></button>
      
      </p-fieldset>
    </div>
    <div *ngIf="authService.isLoggedIn">
      <div>Logout bestätigen</div>
      <button pButton type="button" label="Logout" (click)="logout()" *ngIf="authService.isLoggedIn"></button>
    </div>
    
    `
})

export class LoginComponent implements OnInit {
  msgs = [];
  username: string;
  password: string;
  //loginFailed: boolean = false;

  constructor(public authService: AuthService, public router: Router) {}

  ngOnInit() {
    this.msgs = [];
  }

  login() {
    this.msgs = [];

    this.authService.login(this.username.trim(), this.password).subscribe(() => {
      
      if (this.authService.isLoggedIn) {
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/home';

        if (this.authService.user.passwordExpired) {
          redirect = '/changePassword';
        }
        //this.loginFailed = false;;
        
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        
        // Redirect the user
        this.router.navigate([redirect]);
      }
      else {
        //this.loginFailed = true;
        this.msgs.push({severity:'error', summary:'Benutzer oder Passwort ungültig.', detail:''});
      }
    });
  }
  logout() {
    this.authService.logout();
  }
  forgotten() {
    this.msgs = [];
    this.msgs.push({severity:'info', summary:'bitte warten...', detail:''});

    this.authService.passwortForgotten(this.username).subscribe(
      result => {
        this.msgs.push({severity:'info', summary:result.msg, detail:''});
      },
      error => {
        let body = error.json();
        this.msgs = [];
        this.msgs.push({severity:'error', summary:body.msg, detail:''});
      }
    );
  }
}
