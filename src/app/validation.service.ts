import { Injectable }       from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldBase } from './meta';


@Injectable()
export class ValidationService {
	getFieldValidator(fields:FieldBase[]) {
		return new FieldValidator(fields);
	}
}

export class FieldValidator {
	result: ValidationResult;

	constructor(private fields:FieldBase[]) {
		this.result = new ValidationResult();

		this.resetErrors();
	}

	private resetErrors() {
		this.fields.forEach((f:FieldBase) => {
			this.result.formErrors[f.name] = '';
		});
	}

	validate(form: FormGroup) {
		
		this.fields.forEach((f:FieldBase) => {
			const control = form.get(f.name);
			if (!control.valid) {
				for (const key in control.errors) {
		          this.result.formErrors[f.name] += this.validationMessages[key] + ' ';
		        }
			}
		});
	}

	validationMessages = {
      'required':      'Pflichtfeld',
      'minlength':     'Mindestens %1 Zeichen eingeben',
      'maxlength':     'Maximal %1 Zeichen erlaubt'
    };
  
}

export class ValidationResult {
	formErrors : {[key:string] : string} = {};
}