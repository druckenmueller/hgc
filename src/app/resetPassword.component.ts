import { Component, OnInit }   from '@angular/core';
import { Router, ActivatedRoute }      from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  template: `
      <p-messages [value]="msgs"></p-messages>
      <p-fieldset legend="Passwort ändern">
    
      <div class="ui-g">
        <span class="ui-g-12"><label>Neues Passwort</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="password" required></span>
        <span class="ui-g-12"><label>bestätigen</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="passwordConfirm" required (keyup)="onPasswordConfirmChanged()"></span>
      </div>
      
      <button pButton type="button" [disabled]="!passwordConfirmMatch" label="Passwort ändern" (click)="resetPassword()"></button>
      

      </p-fieldset>
    
    
    `
})

export class ResetPasswordComponent implements OnInit {
  password: string;
  passwordConfirm: string;
  passwordConfirmMatch: boolean = false;
  msgs = [];
  

  constructor(public authService: AuthService, public router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.msgs = [];
  }

  onPasswordConfirmChanged() {
    this.passwordConfirmMatch = this.password == this.passwordConfirm;
  }

  resetPassword() {
    this.authService.resetPassword(this.password, this.route.snapshot.params['token']).subscribe(
      result => {
        this.router.navigate(['/home']);
      },
      error => {
        let body = error.json();
        this.msgs.push({severity:'error', summary:body.msg, detail:''});
      }
      
    );
  }
  
}
