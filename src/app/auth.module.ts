import { NgModule, Injectable } from '@angular/core';
import { Http, RequestOptions, Request, RequestOptionsArgs , Response, Headers, XHRBackend } from '@angular/http';
import { AuthConfig } from 'angular2-jwt';
import { Observable }     from 'rxjs/Observable';
import { JwtConfigService, JwtHttp } from 'angular2-jwt-refresh';
import { Config } from './config';
import { HttpService } from './http.service';
import { AuthService } from './auth.service';

@NgModule({
  providers: [
    {
      provide: HttpService,
      useFactory: getHttpService,
      deps: [ Http, AuthService ]
    }
  ]
})
export class AuthModule {}



export function getHttpService(http: Http, authService: AuthService, options: RequestOptions) {
  let jwtOptions = {
    endPoint: Config.serverUrl + '/token', 
    // optional
    payload: { type: 'refresh', refreshToken :  sessionStorage.getItem(AuthService.REFRESH_TOKEN)},
    beforeSeconds: 60, // refresh tokeSn before 10 min
    tokenName: AuthService.REFRESH_TOKEN,
    refreshTokenGetter: (() => sessionStorage.getItem(AuthService.REFRESH_TOKEN)),
    tokenSetter: ((res: Response): boolean | Promise<void> => {
      //res = res.json();
      let tokens = res.json();
      console.log('tokens: ' + JSON.stringify(tokens));
 
      if (!tokens.token) {
        sessionStorage.removeItem(AuthService.ACCESS_TOKEN);
        sessionStorage.removeItem(AuthService.REFRESH_TOKEN);
 
        return false;
      }
 
      sessionStorage.setItem(AuthService.ACCESS_TOKEN, tokens.token.accessToken);
      sessionStorage.setItem(AuthService.REFRESH_TOKEN, tokens.token.refreshToken);
 
      return true;
    })
  };
  let authConfig = new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => sessionStorage.getItem(AuthService.ACCESS_TOKEN)),
  });
 
  return new HttpService(
    new JwtConfigService(jwtOptions, authConfig),
    http,
    options,
    authService
  );
}
