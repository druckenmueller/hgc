import { Meta, ChildObject, Layout, FormulaField, FieldBase } from './meta';

import * as moment from 'moment/moment';

import { Foo } from './formulaFunctions';

export class CO {
	private raw:any;
	private isDirty:boolean;
	private _meta:Meta;

	constructor(o:any, type?:any, recordtype?:any, metaKey?: any) {
		this.raw = o;
		this.raw.__type = this.raw.__type || type;
		this.raw.__recordtype = this.raw.__recordtype || recordtype;
		this.raw.__metaKey = this.raw.__metaKey || metaKey;

		if (this.raw.__id == undefined) {
			this.raw.__id = this.raw.__metaKey + this.generateQuickGuid();
		}

		this.isDirty = false;
	}

	private caclulateFormulas() {
		for(var i=0; i<this._meta.fields.length; i++) {
			var field = this._meta.fields[i];

			if (field.type == 'formula') {
				var f = new Function("o","return " + field.formula);
				var value = f(this.raw);
				this.raw[field.name] = this._meta.field[field.name].formatValue(value);
			}
		}
		
	}

	set meta(meta:Meta) {
		this._meta = meta;
		this.caclulateFormulas();
	}

	get meta() {
		return this._meta;
	}

	get hasDbId(): boolean {
		return this.raw.hasOwnProperty('_id');
	}

	get id():string {
		return this.raw['__id'];
	}

	get type():string {
		return this.raw['__type'];
	}

	get recordtype():string {
		return this.raw['__recordtype'];
	}

	set recordtype(name:string) {
		this.raw['__recordtype'] = name;
	}

	get createdById():string {
		return this.raw['createdById'];
	}

	get createdDate():string {
		return this.raw['createdDate'];
	}

	get lastModifiedById():string {
		return this.raw['lastModifiedById'];
	}

	get lastModifiedDate():Date {
		return moment(this.raw['lastModifiedDate']).toDate();
	}

	getAttribute(name:string):string {
    	return this.raw.hasOwnProperty(name) ? this.raw[name] : '';
  	}

  	setAttribute(name:string, value:string) {
  		this.raw[name] = value;
  		this.isDirty = true;
  	}

  	hasAttribute(name:string):boolean {
  		return this.raw.hasOwnProperty(name);
  	}

  	get toJSON():any {
  		return this.raw;
  	}

  	setSystemFields(operation:string, user: any) {
  		this.raw.lastModifiedDate = new Date().toISOString();
		this.raw.lastModifiedById = user.__id;

  		if (operation == 'insert') {
  			this.raw.createdDate = new Date().toISOString();
			this.raw.createdById = user.__id;
  		}

  		if (operation == 'delete') {
  			this.raw.deletedDate = new Date().toISOString();
  		}
  	}

  	private generateQuickGuid() {
	    return Math.random().toString(36).substring(2, 15) +
	        Math.random().toString(36).substring(2, 15);
	}

	
}



