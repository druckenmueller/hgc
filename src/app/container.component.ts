import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

import { Observable }     from 'rxjs/Observable';

import {MenuItem} from 'primeng/primeng';
import {Message} from 'primeng/primeng';

import { CO } from './co';
import { Meta } from './meta';
import { COService } from './COService';
import { MetaService } from './meta.service';
import { FieldService } from './FieldService';
import { OfflineService } from './offline.service';
import { ValidationService } from './validation.service';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
 
  template: `
    <div *ngIf="initialized">
      <p-growl [value]="msgs" [life]="3000"></p-growl>
      <p-confirmDialog header="Bestätigung" icon="fa fa-question-circle" width="425"></p-confirmDialog>
      <p-toolbar>
        <div class="ui-toolbar-group-left">
          <div *ngIf="authService.user">
            <span>
              Willkommen {{authService.user.firstname}}
            </span>
          </div>
          <div style="font-size:x-small">
            <span *ngIf="authService.user.lastSuccessfulLogin">Letzter erfolgreicher Login: <t-date [isInput]=false [dateString]="authService.user.lastSuccessfulLogin"></t-date></span>
            <span *ngIf="authService.user.lastFailedLogin">Letzter fehlgeschlagener Login: <t-date [isInput]=false [dateString]="authService.user.lastFailedLogin"></t-date></span>
          </div>
        </div>
        <div class="ui-toolbar-group-right">
          <span>
            <input type="text" [(ngModel)]="searchTerm" placeholder="suche"/>
            <button pButton type="button" label="ok" (click)="onSearch()"></button>
          </span>
          <span *ngIf="authService.user">
            <a routerLink="/logout" routerLinkActive="active">Logout</a>
          </span>
        </div>
      </p-toolbar>
      <!--
      <p-toolbar>
        <div class="ui-toolbar-group-left">
          <input type="text" placeholder="suche"/>
          <button pButton type="button" label="ok" (click)="onSearch()"></button>
        </div>
        <div class="ui-toolbar-group-right">
          <span *ngIf="offline.online">
            Anwendung ist <span style="color:green">online</span>.
          </span>
          <span *ngIf="!offline.online">
            Anwendung ist <span style="color:red">offline</span>.
          </span>
          <span *ngIf="offline.getRecordCount() > 1">
            Es sind {{offline.getRecordCount()}} Datensätze offline verfügbar.
          </span>
          <span *ngIf="offline.getRecordCount() == 1">
            Es ist ein Datensatz offline verfügbar.
          </span>
          <span *ngIf="offline.getRecordCount() == 0">
            Es sind keine Datensätze offline verfügbar.
          </span>
        </div>
        
      </p-toolbar>
      -->
      <p-tabMenu [model]="items"></p-tabMenu>
    </div>
    <router-outlet></router-outlet>
    
  `,
  providers: [COService, FieldService, MetaService, OfflineService, ValidationService]
  
})
export class ContainerComponent implements OnInit {
  msgs: Message[] = [];

  items: MenuItem[] = [];
  activeItem: MenuItem;

  appConfig: any;
  
  initialized: boolean = false;
  searchTerm: string;

  constructor(
    private service: COService,
    private metaService: MetaService,
    private offline: OfflineService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) {
  }

  loadInitData() : Observable<boolean> {
        return this.metaService.getAppConfig()
            .flatMap(appConfigResponse => {
                this.appConfig = appConfigResponse;
                console.log('appConfig loaded: ' + appConfigResponse);
                return this.metaService.findAllMeta()
            }).flatMap(response => {
                console.log('metaData loaded => init done');
                return Observable.of(true);
            })
    }

  ngOnInit() {
    this.loadInitData().subscribe(response => {
      console.log('init data ' + JSON.stringify(this.appConfig));

      this.items.push({
        label: 'Home',
        icon: 'fa-home',
        routerLink: ['/home']
      });

      this.appConfig.tabs.forEach((cobjectType:string) => {
        let meta:Meta = this.metaService.metaByType[cobjectType];
        let label: string = meta.labelPlural;
        let icon: string = meta.icon;
        let link = ['/list/' + cobjectType];
        let item = {label: label, icon: icon, routerLink: link};

        this.items.push(item);
      });

      this.activeItem = this.items[0];

      console.log('items: ' + this.items);

      this.initialized = true;
    });
  }

  onSearch() {
    this.router.navigate(['/search', this.searchTerm]);
  }
}