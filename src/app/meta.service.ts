import { Injectable }       from '@angular/core';
import { Http, Response }   from '@angular/http';
import { Headers }          from '@angular/http';

import { Observable }     from 'rxjs/Observable';

import { Config } from './config';
import { Meta } from './meta';
import { OfflineService } from './offline.service';

//import { AuthHttp } from 'angular2-jwt';
import { JwtHttp } from 'angular2-jwt-refresh';
import { HttpService } from './http.service';

@Injectable()
export class MetaService {
  private url = Config.serverUrl +  '/api/v1/meta';  // URL to web API
  private keyToType : {[key: string]: string} = {};
  
  metaByType : {[type: string]: Meta} = {};

  constructor (
    private http: HttpService,
    private offline: OfflineService) {
  }
  
  public extractMetaKey(id: string) {
    return id.substr(0,3);
  }

  private rawToMeta(res: any) {
    return res.length == 1 ? new Meta(res[0]) : undefined;
  }

  private rawToMetas(res: any) {
    let metas:Meta[] = [];
    res.forEach((m:any) => {
      metas.push(new Meta(m));
    })
    return metas;
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError (error: Response | any, o: Observable<any>) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);

    // assume the server is not available => get meta data from local storage
    //if (this.offline.hasObject('meta_' + ))
    //this.offline.getObject()

    return Observable.throw(errMsg);
  }


  findMeta(type:string):Observable<Meta> {
    return this.http.get(this.url + '/cobject/' + type)
                    .map(this.extractData)
                    .map(this.rawToMeta)
                    .catch((error: any) => {
                      
                      if (this.offline.hasObject('meta_' + type)) {
                        console.log('meta data for ' + type + ' served from cache')
                        return Observable.of(this.offline.getMetaObject(type));
                      }
                      
                      return Observable.throw('');
                    });
  }

  findMetas(types:string[]):Observable<Meta[]> {
    return this.http.get(this.url + '/cobjects/' + types.join(','))
                    .map(this.extractData)
                    .map((res:{}[]) => {
                      return this.rawToMetas(res);
                    })
  }

  findMetaByKey(key:string):Observable<Meta> {
    let type:string = this.keyToType[key];

    if (type) {
      return this.findMeta(type);
    }
    else {
      return this.findAllMeta()
        .flatMap(response => {
          return this.findMeta(this.keyToType[key]);
        })
    }
  }
  
 
  findAllMeta():Observable<any[]> {
    return this.http.get(this.url + '/cobject')
                    .map(this.extractData)
                    .map(this.rawToMetas)
                    .map((metas:Meta[]) => { 
                      metas.forEach((meta:Meta) => {
                        this.keyToType[meta.key] = meta.type;
                        this.metaByType[meta.type] = meta;
                      });
                      return metas;
                    })
                    .catch(this.handleError);
  }

  getAppConfig():Observable<any> {
    return this.http.get(this.url + '/appConfig')
                    .map(this.extractData)
                    .map((res:{}[]) => {
                      return res.length > 0? res[0] : undefined;
                    })
  }
  
}