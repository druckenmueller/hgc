import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <router-outlet></router-outlet>
  `,
  providers: []
})

export class AppComponent { 
}