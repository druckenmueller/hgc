import { NgModule }     from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

import { ListComponent } from './core/list.component';
import { ViewComponent } from './core/view.component';
import { NewComponent } from './core/new.component';
import { EditComponent } from './core/edit.component';
import { SearchComponent } from './core/search.component';

import { AuthGuard }                from './auth-guard.service';
import { MetaService }      from './meta.service';
import { OfflineService } from './offline.service';


import { ContainerComponent } from './container.component';
/*
const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent }
];
*/

const routes: Routes = [
  { 
    path: '', 
    canActivate: [AuthGuard],
    component: ContainerComponent,
    children: [
      { path: '', component: HomeComponent, pathMatch:'full'},
      { path: 'home', component: HomeComponent},
      { path: 'search/:searchTerm', component: SearchComponent},
      { path: 'list/:type',  component: ListComponent },
      { path: 'view/:id',  component: ViewComponent },
      { path: 'new/:type',  component: NewComponent },
      { path: 'edit/:id',  component: EditComponent } 
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    MetaService,
    OfflineService
  ]
})
export class AppRoutingModule {}