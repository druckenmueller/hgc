import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard }            from './auth-guard.service';
import { AuthService }          from './auth.service';
import { LoginComponent }       from './login.component';
import { ChangePasswordComponent }       from './changePassword.component';
import { ResetPasswordComponent }       from './resetPassword.component';

const loginRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'changePassword', component: ChangePasswordComponent },
  { path: 'reset/:token', component: ResetPasswordComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard
  ],
  declarations: [
    
  ]
})
export class LoginRoutingModule {}
