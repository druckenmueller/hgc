import { Component, OnInit }   from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  template: `
      <p-messages [value]="msgs"></p-messages>
      <p-fieldset legend="Passwort ändern">
    
      <div class="ui-g">
        <span class="ui-g-12"><label>aktuelles Passwort</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="password" required></span>
        <span class="ui-g-12"><label>neues Passwort</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="passwordNew" required></span>
        <span class="ui-g-12"><label>bestätigen</label></span>
        <span class="ui-g-12"><input [type]="'password'" [(ngModel)]="passwordConfirm" required (keyup)="onPasswordConfirmChanged()"></span>
      </div>
      
      <button pButton type="button" [disabled]="!passwordConfirmMatch" label="Passwort ändern" (click)="changePassword()"></button>
      
      
      </p-fieldset>
    
    
    `
})

export class ChangePasswordComponent implements OnInit {
  password: string;
  passwordNew: string;
  passwordConfirm: string;
  passwordConfirmMatch: boolean = false;
  successful: boolean = false;
  
  msgs = [];

  constructor(public authService: AuthService, public router: Router) {}

  ngOnInit() {
    this.msgs = [];
  }

  onPasswordConfirmChanged() {
    this.passwordConfirmMatch = this.passwordNew == this.passwordConfirm;
  }

  changePassword() {
    if (this.password === this.passwordNew) {
      this.msgs.push({severity:'error', summary:'Passwort ungültig', detail:'Bitte wähle ein anderes Passwort.'});
    }
    else {
      this.authService.changePassword(this.password, this.passwordNew).subscribe(
        result => {
          this.msgs.push({severity:'info', summary:'Passwort erfolgreich geändert.', detail:''});
          this.successful = true;
          this.next();
        },
        error => {
          let body = error.json();
          this.msgs.push({severity:'error', summary:body.msg, detail:''});
        }
      );
    }
  }

  next() {
    let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/home';
    this.router.navigate([redirect]);
  }
  
}
