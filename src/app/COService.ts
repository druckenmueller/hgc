import { Injectable,Output, EventEmitter  }       from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable }     from 'rxjs/Observable';
//import { AuthHttp } from 'angular2-jwt';

import { Config } from './config';

import { CO } from './co';
import { Meta, ChildObject, Layout } from './meta';
import { MetaService } from './meta.service';
import { OfflineService } from './offline.service';
import { AuthService } from './auth.service';
import { JwtHttp } from 'angular2-jwt-refresh';
import { HttpService } from './http.service';

@Injectable()
export class COService {
  @Output() objectsSyncedToServer = new EventEmitter<number>();

  //private useServer = true;
  private url = Config.serverUrl +  '/api/v1/';
  private cobjectUrl = this.url + 'cobjects'; 
  private filesUrl = this.url + 'files'; 

  constructor (
    private http: HttpService,
    private offline: OfflineService,
    private metaService: MetaService,
    private authService: AuthService,) {
    offline.onlineStateChanged.subscribe(isOnline => {
      //this.useServer = isOnline;

      if (isOnline) {
        let upsertedObjects = { inserted: 0, updated: 0 };

        let requests = this.offline.getAllObjects().map((co:CO) => {
            return new Promise((resolve) => {
              this.syncToServer(co, upsertedObjects, resolve);
            });
        });

        Promise.all(requests).then(() => 
            this.objectsSyncedToServer.emit(upsertedObjects.inserted + upsertedObjects.updated)
        );

        
      }
    });

    
  }

  private syncToServer(co: CO, upsertedObjects: any, callback) {
    if (co.hasDbId) {
      this.updateCObject(co).subscribe(
        result => {
          if (result.ok) {
            let body = JSON.parse(result._body);
            
            this.offline.setSaved(body.id);
            upsertedObjects.updated = upsertedObjects.updated + 1; 
            callback();
          }
        },
        error => console.log(error)
      );
    }
    else {
      this.insertCObject(co).subscribe(
        result => {
          if (result.ok) {
            let body = JSON.parse(result._body);
            
            this.offline.setSaved(body.id);
            upsertedObjects.inserted = upsertedObjects.inserted + 1; 
            callback();
          }
        },
        error => console.log(error)

      );
    }
  }

  private rawToCOs(raws: {}[], meta: Meta): CO[] {
    let cos:CO[] = [];
    raws.forEach((raw:{}) => {
      let o:CO = new CO(raw);
      o.meta = meta;
      cos.push(o);
    })
    return cos;
  }

  private rawToCO(res: any) {
    return res.length == 1 ? new CO(res[0]) : undefined;
  }

  private extractData(res: Response) {
    //this.offline.setOnlineOffline(true);
    console.log(this.offline);
    let body = res.json();
    return body;
  }
  

  getObjects(type:string):Observable<any[]> {
    let meta:Meta;

    return this.metaService.findMeta(type)
      .flatMap(response => {
        meta = response;
        return this.http.get(this.cobjectUrl + '?type=' + type + '&collection=' + meta.collection)
          .map(this.extractData)
          .map((res:{}[]) => {
            return this.rawToCOs(res, meta);
          })
      })
  }


  getObject(id:string) : Observable<CO>{
    let meta:Meta;

    return this.metaService.findMetaByKey(this.metaService.extractMetaKey(id))
      .flatMap(response => {
        meta = response;
        return this.http.get(this.cobjectUrl + '/' + id + '?collection=' + meta.collection)
          .map(this.extractData)
          .map((raw:any) => {
            let co = (raw.length == 1 ? new CO(raw[0]) : undefined);
            if (co) co.meta = meta;
            return co;
          })
      })
  }


  getCObjectField(id:string, name:string):Observable<CO> {
    return this.http.get(this.cobjectUrl + '/' + id + '/fields/' + name)
                  .map((res:Response) => {
                    this.offline.setOnlineOffline(true);
                    return res.json();
                  })
                  .map(this.rawToCO)
                  .catch((e:any) => {
                    this.offline.setOnlineOffline(false);
                    return Observable.throw({err:e});
                  });
  }

  getFile(cobjectId:string, fileFieldName:string) {
    return this.http.get(this.cobjectUrl + '/' + cobjectId + '/files/' + fileFieldName)
                  .map(file => {
                     console.log('service ready');
                    return file;
                  })
                  .catch((e:any) => {
                    return '';
                  });
  }

  
  searchObjects(searchTerm: string, type: string, searchFields: string[]): Observable<CO[]> {
    let meta:Meta;

    return this.metaService.findMeta(type)
      .flatMap(response => {
        meta = response;

        return this.http.get(this.cobjectUrl 
                          + '/lookups?type=' + type 
                          + '&what=' + searchTerm
                          + '&where=' + searchFields.join()
                          + '&collection=' + meta.collection)
            .map(this.extractData)
            .map((res:{}[]) => {
              return this.rawToCOs(res, meta);
            })
      })
  }

  

  getChildObjects(childType:string, parentFieldName: string, parentId: string): Observable<CO[]> { 
    let meta:Meta;

    return this.metaService.findMeta(childType)
      .flatMap(response => {
        meta = response;

        return this.http.get(this.cobjectUrl 
                          + '/childs?parentId=' + parentId 
                          + '&childField=' + parentFieldName)
          .map(this.extractData)
          .map((res:{}[]) => {
            return this.rawToCOs(res, meta);
          })
      })
  }

  doTextSearch(searchTerm: string): Observable<CO[]> {
    return this.http.get(this.cobjectUrl + '/search?searchTerm=' + searchTerm)
      .map(this.extractData)
      .flatMap(response => {
        let rawObjects:any[] = response;
        
        if (rawObjects.length == 0) {
          return Observable.of([]);
        }
        
        let metaTypes : {[metaType:string]:string} = {};
        rawObjects.forEach((rawObj:any) => metaTypes[rawObj.__type] = rawObj.__type);
        let metaTypeKeys:string[] = [];
        for (var key in metaTypes) {
          metaTypeKeys.push(key);
        }
        return this.metaService.findMetas(metaTypeKeys)
          .flatMap(metas => {
            let metaByType : {[metaType:string]:Meta} = {};
            metas.forEach((meta:Meta) => {metaByType[meta.type] = meta});

            let cobjects:CO[] = [];
            rawObjects.forEach((rawObj:any) => {
                let co = new CO(rawObj);
                co.meta = metaByType[co.type];
                cobjects.push(co);
              });
            
            return Observable.of(cobjects);

          });
      });
  }

  updateCObject(co:CO) : Observable<any> {
    co.setSystemFields('update', this.authService.user);

    // update cache as well to prevent that the cached object is getting out of sync
    if (this.offline.hasObject(co.id)) {
      this.offline.setObject(co);
    }
    
    return this.http.put(this.cobjectUrl + '/' + co.id, co.toJSON)
            .catch((e:any) => {
              this.offline.setOnlineOffline(false);
              this.offline.setObject(co);
              return Observable.of({});
            });
  }

  insertCObject(co:CO) : Observable<any> {
    co.setSystemFields('insert', this.authService.user);

    //if (this.useServer) {
      return this.http.post(this.cobjectUrl + '/' + co.meta.type, co.toJSON).map((r:any) => { 
                        console.log(r);
                        return r;
                     })
                    .catch((e:any) => {
                      this.offline.setOnlineOffline(false);
                      this.offline.setObject(co);
                      return Observable.of({});
                    });
    /*
    }
    else {
      this.offline.setObject(co);
      return Observable.of({});
    }
    */
  }

  deleteCObject(co:CO, meta: Meta) : Observable<any> {
    co.setSystemFields('delete', this.authService.user);

    let childObjectJson: any[] = [];
    


      // TODO
      // update cache as well to prevent that the cached object is getting out of sync
      
    return this.http.delete(this.cobjectUrl + '/' + co.type + '/' + co.id)
            .catch((e:any) => {
              this.offline.setOnlineOffline(false);
              return Observable.of(this.offline.deleteObject(co, childObjectJson));
            });
  }

  insertFile(formData) : Observable<any> {
    return this.http.post(this.filesUrl, formData)
            .catch((e:any) => {
              this.offline.setOnlineOffline(false);
              return Observable.of({});
            });
  }


  makeOfflineAvailable(co:CO) {
    this.offline.setObject(co);

    this.metaService.findMeta(co.type).subscribe(
      meta => {
        let layout = meta.getLayoutByRecordtypeName(co.recordtype);

        layout.childObjects.forEach((childObject: ChildObject) => {

          this.getChildObjects(childObject.name, childObject.refField, co.id).subscribe(
            cobjects => {
              cobjects.forEach((co:CO) => {
                this.makeOfflineAvailable(co);
              });
            },
            error => console.log(error)
          );
        })
      },
      error => console.log(error)
    );
  }

  isOfflineAvailable(id:string) {
    return this.offline.hasObject(id);
  }
  
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  
  
}